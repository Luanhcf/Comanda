<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$descricao='';
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
$id     =    ''; 
$status   =  '';
$datacad   = date("d-m-Y");
$nome     =  ''; 
$apelido  =  '';
$endereco =  '';
$bairro   =  '';
$cidade   =  '';
$cpf      =  ''; 
$rg       =  '';
$celular  =  '';
$email    =  '';
$comissao =  '';
$telefone =  '';
$obs      =  '';
$funcao   =  '';
$descricao= 'Cadastro de Funcionário';
}else{
$id = isset($_GET['id']) ? $_GET['id'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from funcionarios where id = $id";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);

//Variaveis retornando do banco de dados
$status = trim($row['status']); //
$datacad = trim($row['datacad']);
$nome = trim($row['nome']);
$apelido = trim($row['apelido']);
$endereco = trim($row['endereco']);
$bairro = trim($row['bairro']);
$cidade = trim($row['cidade']);
$cpf = trim($row['cpf']);
$rg = trim($row['rg']);
$celular = trim($row['celular']);
$email = trim($row['email']);
$comissao = trim($row['comissao']);
$telefone = trim($row['telefone']);
$obs = trim($row['obs']);
$funcao = trim($row['funcao']);
$descricao= 'Edição Funcionário';
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Inserir Funcionário</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

</head>

<body>
<form  name="cad_func" method="post" action="../../rec/funcajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?>Manda</a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>
      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp<?php echo $descricao; ?></h3>
  <br>
  <div class="col-md-10">
  <div class="form-row">
    <div class="form-group col-md-1">
    <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
      <label>Codigo</label>
      <input type="text" class="form-control form-control-sm" id="id" value="<?php echo $id; ?>" readonly="true" name="id">
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-1">
      <label for="pessoa">Status</label>
      <select id="status" class="form-control form-control-sm" value="<?php echo $status; ?>" name="status">
        <option value="t" <?php echo trim($status) == "t" ? "selected" : "" ?>>Ativo</option>
        <option value="f" <?php echo trim($status) == "f" ? "selected" : "" ?>>Inativo</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="pessoa">Função</label>
      <select id="funcao" class="form-control form-control-sm" value="<?php echo $funcao; ?>" name="funcao">
        <option value="Garcom" <?php echo trim($funcao) == "Garcom" ? "selected" : "" ?>>Garcom</option>
        <option value="Cozinha" <?php echo trim($funcao) == "Cozinha" ? "selected" : "" ?>>Cozinha</option>
        <option value="Caixa" <?php echo trim($funcao) == "Caixa" ? "selected" : "" ?>>Caixa</option>
        <option value="Outros" <?php echo trim($funcao) == "Outros" ? "selected" : "" ?>>Outros</option>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label>Data Cadastro</label>
      <input type="text" class="form-control form-control-sm" id="datacad" readonly="true" value="<?php echo $datacad; ?>" name="datacad">
    </div>  
  </div>
  <div class="form-row">
  <div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control form-control-sm" id="nome"  name="nome" required value="<?php echo $nome; ?>" maxlength="50" >
  </div>
  <div class="form-group col-md-3">
      <label>Apelido</label>
      <input type="text" class="form-control form-control-sm" id="apelido" required value="<?php echo $apelido; ?>" name="apelido" maxlength="50">
    </div>
</div>
<div class="form-row">
  <div class="form-group col-md-3">
    <label>Endereço</label>
    <input type="text" class="form-control form-control-sm" id="endereco" name="endereco" value="<?php echo $endereco; ?>" maxlength="50">
  </div>
  <div class="form-group col-md-2">
    <label>Bairro</label>
    <input type="text" class="form-control form-control-sm" id="bairro" name="bairro" value="<?php echo $bairro; ?>" maxlength="25">
  </div>
  <div class="form-group col-md-2">
    <label>Cidade</label>
    <input type="text" class="form-control form-control-sm" id="cidade" name="cidade" value="<?php echo $cidade; ?>" maxlength="25" >
  </div>
</div>
  <div class="form-row">
    <div class="form-group col-md-1.4">
      <label>CPF</label>
      <input type="text" class="form-control form-control-sm" id="cpf" name="cpf" value="<?php echo $cpf; ?>" onKeyPress="formata_mascara(this,'###.###.###-##','#')" maxlength="14">
    </div>
    <div class="form-group col-md-1.4">
      <label>R.G</label>
      <input type="text" class="form-control form-control-sm" id="rg" name="rg" value="<?php echo $rg; ?>" maxlength="14">
    </div>
    <div class="form-group col-md-1.4">
      <label>Celular</label>
      <input type="text" class="form-control form-control-sm" id="celular" name="celular" value="<?php echo $celular; ?>" maxlength="14" onKeyPress="formata_mascara(this,'(##)#####-####','#')" placeholder="(86)99999-9999">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-3">
      <label>E-mail</label>
      <input type="text" class="form-control form-control-sm" id="email" name="email" value="<?php echo $email; ?>" maxlength="50">
    </div>
  <div class="form-group col-md-2">
    <label>Comis %</label>
    <input type="text" class="form-control form-control-sm" id="comissao" name="comissao" value="<?php echo $comissao; ?>" maxlength="25" >
  </div> 
  <div class="form-group col-md-2">
      <label>Telefone</label>
      <input type="text" class="form-control form-control-sm" id="telefone" name="telefone" value="<?php echo $telefone; ?>" maxlength="13" onKeyPress="formata_mascara(this,'(##)####-####','#')" placeholder="(86)3333-3333">
    </div> 
  </div>
  <div class="form-row">
  <div class="form-group col-md-7">
    <label>Observação</label>
    <textarea class="form-control form-control-sm" id="obs" name="obs" rows="2" maxlength="1000"><?php echo $obs; ?> </textarea>
  </div>
</div>
<button type="submit" class="btn btn-secondary">Gravar</button>
<a href="func_grid.php?operacao=ativos"><button type="button" class="btn btn-secondary">Voltar</button></a>
  </form>
</body>
</html>