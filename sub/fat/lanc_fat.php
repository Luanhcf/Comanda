<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$userlogado = $_SESSION["usuario"];
$sqlvernivel = "select bloquea_op,nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$namesis = $_SESSION["namesis"];
$idmesa = isset($_GET['idmesa']) ? $_GET['idmesa'] : '';
$erro1 = isset($_GET['erro1']) ? $_GET['erro1'] : '';
$msg1="<div class=\"alert alert-danger\"><strong>Produto sem saldo de estoque</strong><br>A venda não é possível.</div>";
$id = isset($_GET['id']) ? $_GET['id'] : '';
$prc_venda = isset($_GET['prc_venda']) ? $_GET['prc_venda'] : '';
$codprod = isset($_GET['codprod']) ? $_GET['codprod'] : '';
$addprod = isset($_GET['addprod']) ? $_GET['addprod'] : '';
$excluir = isset($_GET['excluir']) ? $_GET['excluir'] : '';
$detalhe = isset($_GET['detalhe']) ? $_GET['detalhe'] : ''; 
$prodel = isset($_GET['prodel']) ? $_GET['prodel'] : '';
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$funci = $_SESSION["func"];
$proid='';
$prodetalhe='';
$proidins='';
$prodetalheins='';
//                                                      verificando os parametros para setar cliente padrão;
$sql4 = "select * from params";
$res4 = pg_query($conexao, $sql4);
$row2 = pg_fetch_assoc($res4);
$cli_pad = $row2['cli_pad'];
$taxas= $row2['taxas'];
//                                                        select para montagem do grid de classificação.
$sql5 = "select substring(descricao from 1 for 15) as descricaoo,* from classificacao where status='t'";
$res5 = pg_query($conexao,$sql5);
$gridclass = "";
$cont =0;
//                                                          select para montagem do grid de produtos.
$class = isset($_GET['class']) ? $_GET['class'] : '0';
$sql6 = "select round(pr_venda,2) as venda, substring(descricao from 1 for 20) as descricaoo,* From produtos where class=$class and status='t'";
$res6 = pg_query($conexao,$sql6);
$gridprod = "";
$cont1 =0;
                    //                                                  VERIFICAR ULTIMO NUMERO DE ATENDIMENTO DA MESA
$sql7="select atend as ultimon from mesas where id=$idmesa";

$res7 = pg_query($conexao, $sql7);
$ultimo = pg_fetch_assoc($res7);
$ultimon = $ultimo['ultimon'];
//                                                                ADICIONANDO PRODUTOS NA MESA SQL PARA LISTAR O CHAMADO DA MESA
if($addprod == 'sim'){
  //SQL PARA VERIFICAR SE OS PRODUTOS POSSUEM ESTOQUE E SE CONTROLAM ESTOQUE
  $sqlestoque="select * From produtos where id=$codprod";
  $resestoque=pg_query($conexao,$sqlestoque);
  $assoestoque=pg_fetch_assoc($resestoque);
 //TESTE PARA VERIFICAR SE PRODUTO CONTROLE ESTOQUE E SE POSSUI ESTOQUE
  if($assoestoque["tipo"] == 'P' && $assoestoque["estoque"] > 0) {

  $sqlcozinha="select pro.id,pro.descricao,clas.id,clas.alerta From produtos pro inner join classificacao clas on (pro.class=clas.id) where pro.id=$codprod";
  $resco=pg_query($conexao,$sqlcozinha);
  $assoc=pg_fetch_assoc($resco);
  $sql8 ="insert into mov_sai (id_mesa,atend,produtos,quant,func,total,cliente,data) values ($idmesa,$ultimon,$codprod,1,$funci,$prc_venda,$cli_pad,current_timestamp)";
  $inspro = pg_query($conexao,$sql8);
  $sqlest="update produtos set estoque=estoque-1 where id=$codprod";
  $resest= pg_query($conexao,$sqlest);
  $sqlras="insert into raster (codigo,tipo,data,quant,ajust,atend) values ($codprod,'S',current_timestamp,1,'f',$ultimon)";
  $resras= pg_query($conexao,$sqlras);
  if($assoc['alerta'] == 't'){
    $inspedido="insert into pedidos (codprod,id_mesa,atend,finaliza,func,quant,data) values ($codprod,$idmesa,$ultimon,'f',$funci,'1',current_timestamp)";
    $inserepedido=pg_query($conexao,$inspedido);
  }
  }
  //TESTE PARA ALERTAR QUANDO O PRODUTO NÃO POSSUIR ESTOQUE E ALERTAR O USUARIO.
  if($assoestoque["tipo"] == 'P' && $assoestoque["estoque"] <= 0) {
    
    header("Location: lanc_fat.php?erro1=1&idmesa=$idmesa" );
    exit;
  }
  //VENDER SEM ESTOQUE.
  if($assoestoque["tipo"] == 'N') {
  $sqlcozinha="select pro.id,pro.descricao,clas.id,clas.alerta From produtos pro inner join classificacao clas on (pro.class=clas.id) where pro.id=$codprod";
  $resco=pg_query($conexao,$sqlcozinha);
  $assoc=pg_fetch_assoc($resco);
  if($assoc['alerta'] == 't'){
    $inspedido="insert into pedidos (codprod,id_mesa,atend,finaliza,func,quant,data) values ($codprod,$idmesa,$ultimon,'f',$funci,'1',current_timestamp)";
    $inserepedido=pg_query($conexao,$inspedido);
  }
  $sql8 ="insert into mov_sai (id_mesa,atend,produtos,quant,func,total,cliente,data) values ($idmesa,$ultimon,$codprod,1,$funci,$prc_venda,$cli_pad,current_timestamp)";
  $inspro = pg_query($conexao,$sql8);
 // $sqlras="insert into raster (codigo,tipo,data,quant,ajust,atend) values ($codprod,'S',current_timestamp,1,'f',$ultimon)";
 // $resras= pg_query($conexao,$sqlras);
}
}
//                                                        INSERIR PRODUTOS PELO BOTÃO DE DETALHAMENTO
  if($addprod == 'item'){
    if($codprod > 0){
      $proid="produtos='$codprod'";
      $proidins ="produtos";
      $prodetalhe="";
      $prodetalheinsert="";
    }
    if($detalhe != '' && $codprod == ''){
      $prodetalhe="detalhe='$detalhe'";
      $prodetalheins="detalhe";
      $prodetalheinsert="upper(detalhe)";
    }
      //SQL PARA VERIFICAR SE OS PRODUTOS POSSUEM ESTOQUE E SE CONTROLAM ESTOQUE
  $sqlestoque="select * From produtos where id=$codprod";
  $resestoque=pg_query($conexao,$sqlestoque);
  $assoestoque=pg_fetch_assoc($resestoque);

 //TESTE PARA VERIFICAR SE PRODUTO CONTROLE ESTOQUE E SE POSSUI ESTOQUE
  if($assoestoque["tipo"] == 'P' && $assoestoque["estoque"] > 0) {
  $sql12="insert into mov_sai (id_mesa,atend,$proidins $prodetalheins,quant,func,total,cliente,data) 
  select id_mesa,me.atend,$proidins $prodetalheinsert,quant,func,total,cliente,current_timestamp 
  From mesas me inner join mov_sai mo on (me.id=mo.id_mesa) where me.id=$idmesa and ocupada='t' and $proid $prodetalhe limit 1";
  $insitem = pg_query($conexao,$sql12);
  $sqlest="update produtos set estoque=estoque-1 where id=$codprod";
  $resest= pg_query($conexao,$sqlest);
  $sqlras="insert into raster (codigo,tipo,data,quant,ajust,atend) values ($codprod,'S',current_timestamp,1,'f',$ultimon)";
  $resras= pg_query($conexao,$sqlras);
  $sqlcozinha="select pro.id,pro.descricao,clas.id,clas.alerta From produtos pro inner join classificacao clas on (pro.class=clas.id) where pro.id=$codprod";
  $resco=pg_query($conexao,$sqlcozinha);
  $assoc=pg_fetch_assoc($resco);
  if($assoc['alerta'] == 't'){
    $inspedido="insert into pedidos (codprod,id_mesa,atend,finaliza,func,quant,data) values ($codprod,$idmesa,$ultimon,'f',$funci,'1',current_timestamp)";
    $inserepedido=pg_query($conexao,$inspedido);
  }
  }
  if($assoestoque["tipo"] == 'P' && $assoestoque["estoque"] <= 0) {
    
    header("Location: lanc_fat.php?erro1=1&idmesa=$idmesa" );
    exit;
  }
  if($assoestoque["tipo"] == 'N') {
    $sql12="insert into mov_sai (id_mesa,atend,$proidins $prodetalheins,quant,func,total,cliente,data) 
    select id_mesa,me.atend,$proidins $prodetalheinsert,quant,func,total,cliente,current_timestamp 
    From mesas me inner join mov_sai mo on (me.id=mo.id_mesa) where me.id=$idmesa and ocupada='t' and $proid $prodetalhe limit 1";
    $insitem = pg_query($conexao,$sql12);
    $sqlcozinha="select pro.id,pro.descricao,clas.id,clas.alerta From produtos pro inner join classificacao clas on (pro.class=clas.id) where pro.id=$codprod";
    $resco=pg_query($conexao,$sqlcozinha);
    $assoc=pg_fetch_assoc($resco);
    if($assoc['alerta'] == 't'){
      $inspedido="insert into pedidos (codprod,id_mesa,atend,finaliza,func,quant,data) values ($codprod,$idmesa,$ultimon,'f',$funci,'1',current_timestamp)";
      $inserepedido=pg_query($conexao,$inspedido);
  }
  }
}
  //                                                               REMOVER ITEM DA LISTAGEM DETALHAMENTO
  if($excluir == 'item'){
    if($prodel > 0){
      $proid="produtos='$prodel'";
    }
    if($detalhe != '' && $prodel == ''){
      $prodetalhe="detalhe='$detalhe'";
    }
  $sqlestoque="select * From produtos where id=$prodel";
  $resestoque=pg_query($conexao,$sqlestoque);
  $assoestoque=pg_fetch_assoc($resestoque);

    if($assoestoque["tipo"] == 'P') {
   $sql11= "delete from mov_sai where id_mesa=$idmesa and $proid $prodetalhe and id=(select max(id) from mov_sai where id_mesa=$idmesa and $proid $prodetalhe)";
   $res11 = pg_query($conexao,$sql11);
   $sqlest="update produtos set estoque=estoque+1 where id=$prodel";
   $resest= pg_query($conexao,$sqlest);
   $sqlras="insert into raster (codigo,tipo,data,quant,ajust,atend) values ($prodel,'C',current_timestamp,1,'f',$ultimon)";
   $resras= pg_query($conexao,$sqlras);
  }
    if($assoestoque["tipo"] == 'N') {
    $sql11= "delete from mov_sai where id_mesa=$idmesa and $proid $prodetalhe and id=(select max(id) from mov_sai where id_mesa=$idmesa and $proid $prodetalhe)";
    $res11 = pg_query($conexao,$sql11);
   }
}

  if($operacao == 'iniciar'){
  //                                            Verificando proxima sequencia para adicionar nas tabelas de mesa e mov_sai  
  $sql= "select nextval('seq_atend') as atendi";
  $res = pg_query($conexao, $sql);
  $row    = pg_fetch_assoc($res);
  $numat = $row['atendi'];
  //                                                     update na tabela mesas para definir que está ocupada
  $sql1 = "update mesas set ocupada='t',atend=$numat where id=$idmesa"; 
  $vai = pg_exec($conexao, $sql1);
  $sql3 = "select * from mov_sai";
  $res2 = pg_query($conexao, $sql3);
  $row1 = pg_fetch_assoc($res2);
  //                                              primeiro movimento após abertura para indicar que esta ocupada
  $sql4 = "insert into mov_sai (id_mesa,atend,func,cliente,data) values ($idmesa,$numat,$funci,$cli_pad,current_timestamp)";
  $res3 = pg_query($conexao, $sql4);
  //var_dump  ($sql4);
  }
  //                                                  select para montagem do grid de detalhamento da conta
$sql9 = " select pro.id,
CASE
  WHEN mo.produtos > 0 THEN pro.descricao
  WHEN mo.produtos IS NULL THEN Trim(mo.detalhe)
  ELSE ''
END                                AS descricao,
Sum(mo.quant)                      AS quant,
Max(mo.total)                      AS unit,
Round(Sum(mo.quant * mo.total), 2) AS total
FROM   mov_sai mo
LEFT JOIN produtos pro
       ON ( mo.produtos = pro.id )
WHERE  mo.id_mesa = $idmesa
AND mo.atend = (SELECT Max(atend)
                FROM   mov_sai
                WHERE  id_mesa = $idmesa)
AND mo.quant > 0
GROUP  BY 1,2 ;";
$res9 = pg_query($conexao,$sql9);
$gridetalhe = "";
//                                                                 SQL PARA EXIBIR O TOTAL DA MESA
$sql10 = "select round(sum(total * quant),2) as total from mov_sai mo where mo.id_mesa=$idmesa and mo.atend=(select max(atend) from mov_sai where id_mesa=$idmesa) and mo.quant > 0";
$res10 = pg_query($conexao,$sql10);
$totaldel = pg_fetch_assoc($res10);
$totaldetalhe = $totaldel["total"];
$total_taxas = ($taxas * $totaldetalhe) / 100 + $totaldetalhe ;

  if($operacao == 'editar'){
    //                                            Teste para verificar se quem vai operar a mesa e quem abriu.
    $sqlparam="select * from params";
    $paramres=pg_query($conexao,$sqlparam);
    $paramsoc=pg_fetch_assoc($paramres);
     $cons= "select * From mesas m inner join mov_sai mo on (m.atend=mo.atend)  where m.id=$idmesa order by mo.id limit 1";
     $res = pg_query($conexao, $cons);
     $row = pg_fetch_assoc($res);
     if (trim($row['func']) != $funci && $paramsoc['individual'] == 't' ){
        header("Location: grid_fat_mes.php?erro=1");
       exit;
     }
     if ($paramsoc['individual'] == 'f' ){
      header("Location: lanc_fat.php?idmesa=$idmesa");
   }
  }
$msgclass="<b><p align=\"left\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Selecione a Classificação</p></b>";
$msgprod="<b><p align=\"left\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Selecione os Produtos</p></b>";
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Operação</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table thead th{
    border-bottom: 0px;
    font-size: 14px;
  }
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  a:link{
    color:black;
    text-decoration: none;
    font-size: 13px;
  }
  a:visited {
    color: black;
    text-decoration: none;
}
  a:hover {
    color: black;
    text-decoration: none;
}
  a:active {
    color: black;
    text-decoration: none;

    }
.tiraespaco th,.tiraespaco td {
  padding:0;
  text-align:left;

}
.aumentar thead th{
  font-size: 15px;
  color: green;
}

  </style>

</head>

<body>
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"../../menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../../logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/cxa/caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <!-- Desenho do cadastro -->
  <br>
  <?php
  if($rowvernivel['bloquea_op'] == 'f'){
  print("<a href=\"grid_fat_mes.php?operacao=fechar&alerta=sucesso&idmesa=$idmesa\">
  <button type=\"button\" class=\"btn btn-secondary\" onclick=\"if(!confirm('Deseja realmente salvar fechar a conta de mesa  $idmesa com o valor R$:".number_format($total_taxas,2)." ?'))return false;\">Fechar conta
  </button>
  </a>"); 
  } else{
  }
  ?>
  <?php
  if($rowvernivel['bloquea_op'] == 'f'){
  print("<a href=\"operacoes.php?operacoes=desconto&idmesa=$idmesa\"><button type=\"button\" class=\"btn btn-secondary\">Desconto</button></a>");
  } else{
  }
  ?>
  <?php
  if($rowvernivel['bloquea_op'] == 'f'){
  print("<a href=\"operacoes.php?operacoes=acrescimo&idmesa=$idmesa\"><button type=\"button\" class=\"btn btn-secondary\">Acrescimo</button></a>");
} else{
}
  ?>
  <?php
  if($rowvernivel['bloquea_op'] == 'f'){
  print("<a href=\"imprimeconta.php?idmesa=$idmesa\" target=\"_blank\"><button type=\"button\" class=\"btn btn-secondary\">Imprimir</button></a>");
} else{
}
  ?>
  <a href="operacoes1.php?operacoes=fracionar&idmesa=<?php echo $idmesa; ?>"><button type="button" class="btn btn-secondary">Fracionar</button></a>
  <a href="grid_fat_mes.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
  <br><br>
  <?php
  if ($erro1 == 1){
    echo $msg1; 
  }
  ?>
<h3><b>Lançamentos Mesa <?php echo $idmesa ?> Nº <?php echo $ultimon ?></b></h3> 
<?php
$conteudoclass= "<table align=\"left\" style=\"width:20%\" class=\"table\">
      <thead>";
    while ($row3=pg_fetch_assoc($res5)){
    $cont = $cont+1;
    $gridclass.="<th>
    <a href=\"lanc_fat.php?funci=$funci&cli_pad=$cli_pad&idmesa=$idmesa&class=".$row3["id"]."\">
    <img src=\"../../img/".$row3["imagem"]."\" width=\"70px\" height=\"60\">
    <br>
    <label>".$row3["descricaoo"]."
    </label>
    </a>
    </th>";
    
    if($cont == 5){
      $conteudoclass.=("<tr> $gridclass </tr>");
      $gridclass ='';
      $cont= 0;
    }
    
  }
  if($cont > 0){
    $conteudoclass.=("<tr> $gridclass </tr>");
   $gridclass ='';
    $cont= 0;
  }
  $conteudoclass.="</thead>
    </table>";
    ?>
    <?php
   $conteudoprodutos="<table  class=\"table\" align=\"left\" style=\"width:25%\" >
      <thead>";
      //DESENHO DO GRID DE PRODUTOS
   while ($row4=pg_fetch_assoc($res6)){
    $cont1 = $cont1+1;
    $gridprod.="<th><img src=\"../../img/".$row4["imagem"]."\" width=\"90px\" height=\"70\">
    <a href=\"lanc_fat.php?addprod=sim&class=$class&funci=$funci&cli_pad=$cli_pad&idmesa=$idmesa&prc_venda=".$row4["venda"]."&codprod=".$row4["id"]."\">
    <i style=\"color:green;\" class=\"fas fa-plus-circle fa-2x\"></i></a>
    <br>
    <label>".$row4["descricaoo"]."
    <br>
    <font size=\"4\" color=\"blue\">R$:".$row4["venda"]."&nbsp
    </label></th>";
    
    if($cont1 == 4){
      $conteudoprodutos.=("<tr> $gridprod </tr>");
      $gridprod ='';
      $cont1= 0;
    }}
  if($cont1 > 0){
    $conteudoprodutos.=("<tr> $gridprod </tr>");
    $gridprod ='';
    $cont1= 0;
  }
  $conteudoprodutos.="</thead> </table>";
  ?>
     <?php
   $detalhe="<table class=\"table tiraespaco\" cellspacing=\"0\" cellpadding=\"0\">
      <thead>
        <tr>
        <th>Descrição</th>
        <th>Qtd</th>
        <th>Unit</th>
        <th>Total<br>
        </tr>";
      //DESENHO DO GRID DE PRODUTOS DETALHES
   while ($row10=pg_fetch_assoc($res9)){
    $gridetalhe="<tr>
              <th><a href=\"lanc_fat.php?addprod=item&funci=$funci&cli_pad=$cli_pad&idmesa=$idmesa&codprod=".$row10["id"]."&detalhe=".$row10['descricao']."\">
              <i style=\"color:green;\"class=\"fas fa-plus-square fa-lg\"></i></a> &nbsp 
              ".$row10["descricao"]." &nbsp ".($rowvernivel['bloquea_op'] == "f" ? "<a href=\"lanc_fat.php?excluir=item&idmesa=$idmesa&class=$class&prodel=".$row10["id"]."&detalhe=".$row10['descricao']."\"><i style=\"color:red;\"class=\"fas fa-minus-square fa-lg\"></i></a>" : "")." </th>
              <th>".$row10["quant"]."</th>
              <th>".$row10["unit"]."</th>
              <th>".$row10["total"]."</th>
              </tr>";
    $detalhe.=($gridetalhe);
  }
  
     $detalhe.=" </thead>
    </table>";
    ?>
<?php
print(" 
    <table width=\"90%\" border=\"0\" align=\"center\">
    <tr>
        <td width=\"30%\" align=\"left\">
            <div>
                <table width=\"100%\" border=\"0\" align=\"center\">
                <tr>
                    <td width=\"100%\" align=\"center\"> 
                        <div>
                            $msgclass
                            $conteudoclass
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width=\"100%\" align=\"center\"> 
                        <div>
                            $msgprod
                            $conteudoprodutos
                        </div>
                    </td>
                </tr>
                </table>    
            </div>
        </td>
        <td width=\"30%\" align=\"rigth\" valign=\"top\">
        <table width=\"100%\">
        <tr>
        <td colspan=\"4\" width=\"30%\" align=\"left\" valign=\"top\" style=\"color:blue; font-weight:bold;\"> 
        SUB-TOTAL R$:  $totaldetalhe  
        </td>
        </tr>
        <tr>
        <td colspan=\"4\" width=\"30%\" align=\"left\" valign=\"top\" style=\"color:blue; font-weight:bold;\"> 
        TAXA DE SERVIÇO: $taxas %  
        </td>
        </tr>
        <tr>
        <td colspan=\"4\" width=\"30%\" align=\"left\" valign=\"top\" style=\"color:green; font-weight:bold; font-size:20px;\"> 
        TOTAL R$: ".number_format($total_taxas,2)."  
        </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        <tr>
        <td></td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
            <div>
                $detalhe
            </div>
        </td>
    </tr>
    </table>    
    ");
    ?>
  </div>
</body>
</html>