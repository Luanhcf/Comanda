<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$atend = isset($_GET['atend']) ? $_GET['atend'] : '';
//CARREGAR INFORMAÇÕES DA EMPRESA
$sql="select * From empresa";
$res= pg_query($conexao,$sql);
$row= pg_fetch_assoc($res);
$date = date('d/m/Y H:i:s');
//SQL PARA MONTAGEM DO GRIDE
//lpad(substring(trim(pro.descricao), 1, 23), 23, '.'),
$sql1 = "select pro.id,
case 
when mo.produtos > 0 and char_length(pro.descricao) = 1 then pro.descricao||'.......................' 
when mo.produtos > 0 and char_length(pro.descricao) = 2 then pro.descricao||'......................'
when mo.produtos > 0 and char_length(pro.descricao) = 3 then pro.descricao||'.....................'
when mo.produtos > 0 and char_length(pro.descricao) = 4 then pro.descricao||'....................'
when mo.produtos > 0 and char_length(pro.descricao) = 5 then pro.descricao||'...................'
when mo.produtos > 0 and char_length(pro.descricao) = 6 then pro.descricao||'..................'
when mo.produtos > 0 and char_length(pro.descricao) = 7 then pro.descricao||'.................'
when mo.produtos > 0 and char_length(pro.descricao) = 8 then pro.descricao||'................'
when mo.produtos > 0 and char_length(pro.descricao) = 9 then pro.descricao||'...............'
when mo.produtos > 0 and char_length(pro.descricao) = 10 then pro.descricao||'..............'
when mo.produtos > 0 and char_length(pro.descricao) = 11 then pro.descricao||'.............'
when mo.produtos > 0 and char_length(pro.descricao) = 12 then pro.descricao||'............'
when mo.produtos > 0 and char_length(pro.descricao) = 13 then pro.descricao||'...........'
when mo.produtos > 0 and char_length(pro.descricao) = 14 then pro.descricao||'..........'
when mo.produtos > 0 and char_length(pro.descricao) = 15 then pro.descricao||'.........'
when mo.produtos > 0 and char_length(pro.descricao) = 16 then pro.descricao||'........'
when mo.produtos > 0 and char_length(pro.descricao) = 17 then pro.descricao||'.......'
when mo.produtos > 0 and char_length(pro.descricao) = 18 then pro.descricao||'......'
when mo.produtos > 0 and char_length(pro.descricao) = 19 then pro.descricao||'.....'
when mo.produtos > 0 and char_length(pro.descricao) = 20 then pro.descricao||'....'
when mo.produtos > 0 and char_length(pro.descricao) = 21 then pro.descricao||'...'
when mo.produtos > 0 and char_length(pro.descricao) = 22 then pro.descricao||'..'
when mo.produtos > 0 and char_length(pro.descricao) = 23 then pro.descricao||'.'
when mo.produtos > 0 and char_length(pro.descricao) > 24 then substring(pro.descricao from 1 for 24) 
when mo.produtos is NULL and char_length(mo.detalhe) = 1 then trim(mo.detalhe)||'.......................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 2 then trim(mo.detalhe)||'......................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 3 then trim(mo.detalhe)||'.....................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 4 then trim(mo.detalhe)||'....................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 5 then trim(mo.detalhe)||'...................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 6 then trim(mo.detalhe)||'..................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 7 then trim(mo.detalhe)||'..................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 8 then trim(mo.detalhe)||'................' 
when mo.produtos is NULL and char_length(mo.detalhe) = 9 then trim(mo.detalhe)||'...............' 
when mo.produtos is NULL and char_length(mo.detalhe) = 10 then trim(mo.detalhe)||'..............' 
when mo.produtos is NULL and char_length(mo.detalhe) = 11 then trim(mo.detalhe)||'.............' 
when mo.produtos is NULL and char_length(mo.detalhe) = 12 then trim(mo.detalhe)||'............' 
when mo.produtos is NULL and char_length(mo.detalhe) = 13 then trim(mo.detalhe)||'...........' 
when mo.produtos is NULL and char_length(mo.detalhe) = 14 then trim(mo.detalhe)||'..........'
when mo.produtos is NULL and char_length(mo.detalhe) = 15 then trim(mo.detalhe)||'.........' 
when mo.produtos is NULL and char_length(mo.detalhe) = 16 then trim(mo.detalhe)||'........' 
when mo.produtos is NULL and char_length(mo.detalhe) = 17 then trim(mo.detalhe)||'.......' 
when mo.produtos is NULL and char_length(mo.detalhe) = 18 then trim(mo.detalhe)||'......' 
when mo.produtos is NULL and char_length(mo.detalhe) = 19 then trim(mo.detalhe)||'.....' 
when mo.produtos is NULL and char_length(mo.detalhe) = 20 then trim(mo.detalhe)||'....' 
when mo.produtos is NULL and char_length(mo.detalhe) = 21 then trim(mo.detalhe)||'...' 
when mo.produtos is NULL and char_length(mo.detalhe) = 22 then trim(mo.detalhe)||'..' 
when mo.produtos is NULL and char_length(mo.detalhe) = 23 then trim(mo.detalhe)||'.' 
when mo.produtos is NULL and char_length(mo.detalhe) > 24 then substring(mo.detalhe from 1 for 24) 
else '' end as descricao,
sum(mo.quant) as quant,
max(mo.total) as unit,
round(sum(mo.quant * mo.total),2) as total 
from mov_sai mo left join 
produtos pro on (mo.produtos=pro.id) 
where mo.atend=$atend and 
mo.quant > 0 group by 1,2 order by descricao;";
$res1 = pg_query($conexao,$sql1);
$gridetalhe = "";
$cont = 0;
//SQL PARA DETALHAMENTO DOS TOTAIS
$sql2="select id_mesa as mesa,
mo.atend as numero,
(select apelido from funcionarios fun inner join mov_sai mo on (mo.func=fun.id) where mo.atend=$atend limit 1) as funcionario,
(select sum(coalesce(total,0)) as total From mov_sai where atend=$atend and coalesce ( detalhe, '' ) not like 'Taxa%') as total,
coalesce(sum(total),0) as total_taxas,
(select taxas from params as taxas_percent),
(select total From mov_sai where atend=$atend and detalhe like ('Taxa%')) as taxas_vlr,
cli.nome as cliente
From  mov_sai mo
inner join clientes cli on (mo.cliente=cli.id)
where  mo.atend=$atend
group by
id_mesa,
mo.atend,
cli.nome";
$res2 = pg_query($conexao,$sql2);
$row4 = pg_fetch_assoc($res2);

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Impressão conta consulta</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

<style media="print">
    .ext {
        width: 12cm;
        border: 0px solid black;
        font-family:Verdana, Geneva, Tahoma, sans-serif;
        font-size:15px;
    }
</style>
<style>
    .ext {
        width: 12cm;
        border: 0px solid black;
        font-family:Verdana, Geneva, Tahoma, sans-serif;
        font-size:15px;
    }
</style>

</head>

<body>
<?php
    print("
    <table class=\"ext\" align=\"center\">
    <tr>
        <td width=\"100%\" align=\"center\">
            <div>
               ".$row['fantasia']." - ".$row['nome']."
            </div>    
        </td>
    </tr> 
    <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            ".$row['endereco']." - ".$row['cidade']." - ".$row['bairro']."
            </div>
        </td>
      </tr> 
      <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            CNPJ: ".$row['cnpj']." - INSC: ".$row['insc']."
            </div>
        </td>
      </tr> 
      <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            Telefone:".$row['telefone']." Celular:".$row['celular']."
            </div>
        </td>
      </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div style=\"font-size:17px; font-weight:bold;\">
          MESA:".$row4['mesa']." Nº:".$row4['numero']."
          </div>
      </td>
    </tr> 
    <tr> 
    <td width=\"100%\" align=\"center\"> 
        <div>
       --- Não e valido como documento fiscal ---
        </div>
    </td>
  </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
 DESCR. &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp QTD &nbsp&nbsp VL.UNIT &nbsp&nbsp&nbsp TOT &nbsp&nbsp&nbsp
          </div>
      </td>
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr> ");
    print("<table class=\"ext\" align=\"center\">"); 
    while ($row1=pg_fetch_assoc($res1)){
        $gridetalhe="<tr><td >".$row1['descricao']."</td>
                     <td>".$row1['quant']."&nbsp&nbsp&nbsp&nbsp</td>
                     <td>".$row1['unit']."&nbsp&nbsp</td>
                     <td>".$row1['total']."</td></tr>";
                     print($gridetalhe);
     }
    
    print("</table>");
    print("<table class=\"ext\" align=\"center\">
    <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr> 
    <tr>
        <td width=\"100%\" align=\"left\">
            <div style=\"font-weight:bold;\">
              SUB-TOTAL ................................................R$:".$row4['total']." 
            </div>    
        </td>
    </tr> 
    <tr>
    <td width=\"100%\" align=\"left\">
        <div style=\"font-weight:bold;\">
          TAXA DE SERVIÇO    ....................................R$:".$row4['taxas_vlr']." 
        </div>    
    </td>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div style=\"font-weight:bold;\">
      TOTAL GERAL ............................................R$:".$row4['total_taxas']."
    </div>    
</td>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div>
      GARÇOM:".$row4['funcionario']."
    </div>    
</td>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div>
      CLIENTE:".$row4['cliente']."
    </div>    
</td>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div>
     Data: $date
    </div>    
</td>
</tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
<tr>
<td width=\"100%\" align=\"center\">
    <div>
      AGRADEÇEMOS A PREFERENCIA VOLTE SEMPRE!
    </div>    
</td>
</tr>
     </table>");    
    ?>
    
      </table> 

</body>
</html>
