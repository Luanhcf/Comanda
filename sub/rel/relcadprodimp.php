<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis  = $_SESSION["namesis"];
$estoque    = isset($_POST['estoque']) ? $_POST['estoque'] : '';
$status    = isset($_POST['status']) ? $_POST['status'] : '';
$ordena    = isset($_POST['ordena']) ? $_POST['ordena'] : '';
//SQL COM OS DADOS DA EMPRESA
$sql1="select * From empresa";
$res1= pg_query($conexao,$sql1);
$row1= pg_fetch_assoc($res1);
$est = "";
$stats ="";
if($estoque == 'p'){
  $est = "> 0 and";
}else if ($estoque == 's'){
  $est = "= 0 and";
}else if ($estoque == 't'){
  $est = ">= 0 and";
}
if($status == 'T'){
  $stats = "in ('t','f')";
}else if ($status == 't'){
  $stats ="='t'";
}else if ($status == 'f'){
  $stats ="='f'";
}
//SQL DO RELATORIO
$sql2="select id,
              descricao,
              marca,
              complemento,
              und,
              estoque,
              round(pr_custo,2) as pr_custo,
              round(pr_compra,2) as pr_compra,
              round(pr_venda,2) as pr_venda
              From produtos where estoque  $est status $stats order by $ordena";
$res2 = pg_query($conexao,$sql2);
//total preco de compra
$sql3="select sum(estoque) * sum(pr_compra) as total
              From produtos where estoque  $est status $stats";
$res3= pg_query($conexao,$sql3);
$row3= pg_fetch_assoc($res3);   
//total preco de custo
$sql4="select round(pr_custo * estoque,2) as total
              From produtos where estoque  $est status $stats";
$res4= pg_query($conexao,$sql4);
$row7= pg_fetch_assoc($res4);   
//total preco de venda
$sql5="select round(pr_venda * estoque,2) as total
              From produtos where estoque  $est status $stats";
$res5= pg_query($conexao,$sql5);
$row5= pg_fetch_assoc($res5);   
//total estoque
$sql6="select round(sum(estoque),2) as total
              From produtos where estoque  $est status $stats";
$res6= pg_query($conexao,$sql6);
$row6= pg_fetch_assoc($res6);              
?>

<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Movimentação de Caixa</title>

  <meta charset="utf-8"></meta>


  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  .table thead th {

vertical-align: bottom;
border-bottom: 1px solid #000;
border-top: 1px solid #000;
}
.th {
text-align: center;
}
.td {
text-align: center;
}
  </style>

</head>
<body>
<div align="left">
<br>
<h3><b>&nbsp&nbsp<?php echo $row1['fantasia']; ?></b> </h3>
&nbsp&nbsp&nbsp&nbsp<?php echo strtoupper($namesis); ?> - SISTEMA DE GERENCIAMENTO DE MESAS
<br>
<label>&nbsp&nbsp&nbsp RELATORIO DE PRODUTOS CADASTRADOS</label>
</div>  
<table align="center" class="table table-sm" width="50%" >
    <thead>
      <tr>
        <th>CODIGO</th>
        <th>DESCRICAO</th>
        <th>MARCA</th>
        <th>COMP</th>
        <th>UND</th>
        <th>ESTOQUE</th>
        <th>P.COMPRA</th>
        <th>P.CUSTO</th>
        <th>P.VENDA</th>
      </tr>
    </thead>
    <tbody id="myTable1">
    <?php
    $exibicao1 ="";
   while ($row4=pg_fetch_assoc($res2)){
    $exibicao1="<tr>
    <td>".$row4['id']."</td>
    <td>".$row4['descricao']."</td>
    <td>".$row4['marca']."</td>
    <td>".$row4['complemento']."</td>
    <td>".$row4['und']."</td>
    <td>".$row4['estoque']."</td>
    <td>".$row4['pr_compra']."</td>
    <td>".$row4['pr_custo']."</td>
    <td>".$row4['pr_venda']."</td>
    </tr>";
    print("$exibicao1");
  }
  ?>
 <!-- <tr>
  <td> </td>
  <td> </td>
  <td> </td>
  <td> </td>
  <td><b>TOTAL</b></td>
  <td><b><?php echo $row6['total']; ?></b></td>
  <td><b><?php echo $row3['total']; ?></b></td>
  <td><b><?php echo $row7['total']; ?></b></td>
  <td><b><?php echo $row5['total']; ?></b></td>
  </tr> -->
  </table>
</body>


