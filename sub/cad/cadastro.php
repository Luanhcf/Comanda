<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Cadastros</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table thead th{
    border-bottom: 0px;
  }
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  </style>

</head>

<body>
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <br>
  <br>
  <br>

  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>
            <a href="cad_grid.php?operacao=ativos">
              <i
                class="fas fa-user-friends fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Clientes</b>
              </label>
            </a>
          </th>

          <th>
            <a href="forn_grid.php?operacao=ativos">
              <i
                class="fas fa-people-carry fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Fornecedores</b>
              </label>
            </a>
          </th>

          <th>
            <a href="prod_grid.php?operacao=ativos">
              <i
                class="fas fa-box-open fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Mercadorias</b>
              </label>
            </a>
          </th>
          <th>
            <a href="class_grid.php">
              <i
                class="fas fa-sticky-note fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Classificação</b>
              </label>
            </a>
          </th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>
            <a href="func_grid.php?operacao=ativos">
              <i
                class="fas fa-users  fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Funcionarios</b>
              </label>
            </a>
          </td>

          <td>
            <a href="formas_grid.php">
              <i
                class="fas fa-money-check-alt  fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label>
                <b style="color:#696969">Formas de Pagamento</b>
              </label>
            </a>
          </td>

          <td>
            <a href="#">
              <i
                class="far fa-credit-card  fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label>
                <b style="color:#696969">Cartão de Credito</b>
              </label>
            </a>
          </td>

          <td>
            <a href="mesa_grid.php">
              <i
                class="fas fa-table  fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Mesas</b>
              </label>
            </a>
          </td>
        </tr>
        <tr>
          
        <th>
            <a href="caixa_grid.php">
              <i
                class="fas fa-money-bill-alt fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Caixa</b>
              </label>
            </a>
          </th>
         </tr> 
      </tbody>
    </table>
  </div>
</body>
</html>