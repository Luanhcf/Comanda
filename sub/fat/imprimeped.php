<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$id = isset($_GET['id']) ? $_GET['id'] : '';
//CARREGAR INFORMAÇÕES DA EMPRESA
$sql="select * From empresa";
$res= pg_query($conexao,$sql);
$row= pg_fetch_assoc($res);
$date = date('d/m/Y H:i:s');
//SQL PARA MONTAGEM DO GRIDE
//lpad(substring(trim(pro.descricao), 1, 23), 23, '.') as descricao,
$sql1 = "select pe.id,
pe.id_mesa as mesa,
pe.atend as atendimento,
rpad(substring(trim(pro.descricao), 1, 24), 24, '.') as descricao,
fu.apelido,
quant as quantidade,
to_char(data,'dd-mm-yyyy hh24:mi') as data,
to_char(dataf,'dd-mm-yyyy hh24:mi') as dataf,
to_char((case when dataf is null then current_timestamp else dataf end)-data,'hh24:mi') as tempo, 
case when finaliza='t' then 'Atendido' else 'Aguardando' end as atendido
From pedidos pe inner join produtos pro on (pe.codprod=pro.id) inner join funcionarios fu on (pe.func=fu.id) where pe.id=$id
order by dataf desc,pe.id;";
$res1 = pg_query($conexao,$sql1);
$sql2 = "select pe.id,
pe.id_mesa as mesa,
pe.atend as atendimento,
rpad(substring(trim(pro.descricao), 1, 24), 24, '.') as descricao,
fu.apelido,
quant as quantidade,
to_char(data,'dd-mm-yyyy hh24:mi') as data,
to_char(dataf,'dd-mm-yyyy hh24:mi') as dataf,
to_char((case when dataf is null then current_timestamp else dataf end)-data,'hh24:mi') as tempo, 
case when finaliza='t' then 'Atendido' else 'Aguardando' end as atendido
From pedidos pe inner join produtos pro on (pe.codprod=pro.id) inner join funcionarios fu on (pe.func=fu.id) where pe.id=$id
order by dataf desc,pe.id;";
$res2 = pg_query($conexao,$sql1);
$ass1 = pg_fetch_assoc($res2);
$cont = 0;
//SQL PARA DETALHAMENTO DOS TOTAIS
//$sql2="";
//$res2 = pg_query($conexao,$sql2);
//$row4 = pg_fetch_assoc($res2);

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Impressão conta consulta</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

<style media="print">
    .ext {
        width: 12cm;
        border: 0px solid black;
        font-family:Verdana, Geneva, Tahoma, sans-serif;
        font-size:15px;
    }
</style>
<style>
    .ext {
        width: 12cm;
        border: 0px solid black;
        font-family:Verdana, Geneva, Tahoma, sans-serif;
        font-size:15px;
    }
</style>

</head>

<body>
<?php
    print("
    <table class=\"ext\" align=\"center\">
    <tr>
        <td width=\"100%\" align=\"center\">
            <div>
               ".$row['fantasia']." - ".$row['nome']."
            </div>    
        </td>
    </tr> 
    <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            ".$row['endereco']." - ".$row['cidade']." - ".$row['bairro']."
            </div>
        </td>
      </tr> 
      <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            CNPJ: ".$row['cnpj']." - INSC: ".$row['insc']."
            </div>
        </td>
      </tr> 
      <tr> 
        <td width=\"100%\" align=\"center\"> 
            <div>
            Telefone:".$row['telefone']." Celular:".$row['celular']."
            </div>
        </td>
      </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div style=\"font-size:17px; font-weight:bold;\">
          MESA:".$ass1['mesa']." Nº:".$ass1['atendimento']."
          </div>
      </td>
    </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr> 
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
 DESCR. &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp QTD &nbsp&nbsp&nbsp&nbsp DATA-HORA &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          </div>
      </td>
      <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr> ");
    print("<table class=\"ext\" align=\"center\">"); 
    while ($row1=pg_fetch_assoc($res1)){
        $gridetalhe="<tr><td>&nbsp&nbsp".$row1['descricao']."</td>
                     <td>".$row1['quantidade']."&nbsp&nbsp&nbsp&nbsp</td>
                     <td>".$row1['data']."&nbsp&nbsp</td>
                     </tr>";
                     print($gridetalhe);
     }
    
    print("</table>");
    print("<table class=\"ext\" align=\"center\">
    <tr> 
      <td width=\"100%\" align=\"center\"> 
          <div>
      ----------------------------------------------------------------  
          </div>
      </td>
    </tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div>
    &nbsp&nbsp GARÇOM:".$ass1['apelido']."
    </div>    
</td>
</tr>
<tr>
<td width=\"100%\" align=\"left\">
    <div>
    &nbsp&nbsp DATA: $date
    </div>    
</td>
</tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
</tr>
<tr>
<td width=\"100%\" align=\"center\">
    <div>
    _____________________________________<br>
     Ass Cozinha
    </div>    
</td>
</tr>
     </table>");    
    ?>
    
      </table> 

</body>
</html>
