<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$operacoes = isset($_GET['operacoes']) ? $_GET['operacoes'] : '';
$idmesa = isset($_GET['idmesa']) ? $_GET['idmesa'] : '';
$titulo="";

if($operacoes == 'desconto'){
$titulo='desconto';
}

if($operacoes == 'acrescimo'){
$titulo='acréscimo';
}

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $titulo; ?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

 <script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>45 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>

</head>
<html>
<body>
<form  name="cad_classi" method="post" action="opajax.php" enctype="multipart/form-data">
<h2 align="center">Aplicar <?php echo $titulo ?> Mesa: <?php echo $idmesa ?> </h2>
<br>
    <input  name="operacoes" type="hidden" value='<?php echo $operacoes; ?>'/>
    <input  name="idmesa" type="hidden" value='<?php echo $idmesa; ?>'/>
  
  <div class="col-md-10" >
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Detalhe</label>
      <input type="text" class="form-control form-control-sm" id="detalhe"  maxlength="30" name="detalhe">
    </div>
    <div class="form-group col-md-2">
      <label>Valor R$:</label>
      <input type="numeric" class="form-control form-control-sm" onkeypress='return SomenteNumero(event)' id="valor"  name="valor">
    </div>
  </div>
<button type="submit" class="btn btn-secondary">Aplicar</button>
</div>
</form>
  
</body>
</html>