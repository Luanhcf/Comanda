<?php
require_once '../config/conexao.php';
//Variaveis da pagina
$id       = isset($_POST['id']) ? $_POST['id'] : ''; //variavel cadastro de caixa
$descr   = isset($_POST['descr']) ? $_POST['descr'] : ''; //variavel cadastro de caixa
$data   = isset($_POST['data']) ? $_POST['data'] : ''; //Variavel suprimento de caixa
$caixa   = isset($_POST['caixa']) ? $_POST['caixa'] : ''; //Variavel suprimento de caixa
$forma   = isset($_POST['forma']) ? $_POST['forma'] : ''; //Variavel suprimento de caixa
$valor   = isset($_POST['valor']) ? $_POST['valor'] : ''; //Variavel suprimento de caixa
$historico   = isset($_POST['historico']) ? $_POST['historico'] : ''; //Variavel suprimento de caixa

// Teste para inserir
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : ''; // variavel do cadastro e operacoes de caixa

if ($operacao == "novo") {
		$sql = "insert into caixa (descricao) values (upper('$descr'))";
		$res = pg_exec($conexao, $sql);
		header("Location: ../sub/cad/caixa_grid.php");
		exit;

} 

if ($operacao == "editar") {
		$sql = "update caixa set descricao=upper('$descr') where id=$id";
		$res = pg_exec($conexao, $sql);
		header("Location: ../sub/cad/caixa_grid.php");
		exit;
}

if ($operacao == "suprimento"){
		$sql = "insert into mov_cxa (caixa,data,forma,valor,historico,tipo) values ($caixa,'$data',$forma,$valor,'SUPRIMENTO '||upper('$historico'),'E')";
		$res = pg_exec($conexao,$sql);
		header("Location: ../sub/cxa/suprimento.php?alert=2");
		exit;
}

if ($operacao == "sangria"){
	$sql = "insert into mov_cxa (caixa,data,forma,valor,historico,tipo) values ($caixa,'$data',$forma,$valor,'SANGRIA '||upper('$historico'),'S')";
	$res = pg_exec($conexao,$sql);
	header("Location: ../sub/cxa/sangria.php?alert=2");
} 
?>