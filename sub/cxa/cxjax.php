<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$operacao   = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$atend   = isset($_POST['atend']) ? $_POST['atend'] : '';
$caixa   = isset($_POST['caixa']) ? $_POST['caixa'] : '';
$data   = isset($_POST['data']) ? $_POST['data'] : '';
$total   = (double)(isset($_POST['total']) ? $_POST['total'] : '0');
$troco   = (double)(isset($_POST['troco']) ? $_POST['troco'] : '0');

$id="";
$totalvenda = 0;
if($operacao == "lancar"){
	$sqlcxa = "select * from formas order by id";
	$rescxa = pg_query($conexao,$sqlcxa); 
	while($row = pg_fetch_assoc($rescxa)){
		$id = $row['id'];
		$valor   = (double)(isset($_POST["valor$id"]) ? $_POST["valor$id"] : '0');
		
		if($valor > 0){
			$totalvenda=$totalvenda+$valor; 
		}
	}
	
	if(round($totalvenda-$troco,2) != round($total,2)){

		header("Location: movimentacao3.php?alert=1&data=$data&caixa=$caixa&atend=$atend&total=$total");
		exit;
	}
  	$primeiro = true;
	$rescxa = pg_query($conexao,$sqlcxa); 
	while($row = pg_fetch_assoc($rescxa)){
	$id = $row['id'];
	$valor   = (double)(isset($_POST["valor$id"]) ? $_POST["valor$id"] : '0');
	//Retirar o troco da primeira forma de pagamento (sempre o menor id);
	if($primeiro ){
		$primeiro = false;
		if($troco > 0 ){
			$valor = $valor - $troco;
		}
	}
	if($valor > 0){
		$sql= "insert into mov_cxa (caixa,data,forma,valor,historico,tipo,chave_atend) 
		values ($caixa,'$data',$id,$valor,'RECEBIMENTO DA VENDA Nº $atend','E',$atend)";
		$res = pg_query($conexao,$sql);

	}
	
	
    }
	header("Location: movimentacao2.php?alert=1&atend=$atend&caixa=$caixa&data=$data");
}
	
?>