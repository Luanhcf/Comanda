<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$val = isset($_GET['val']) ? $_GET['val'] : '';
$msg7="";
$msg7="<div class=\"alert alert-success\" role=\"alert\">
Sistema validado com sucesso! </div>";
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Configurações</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table thead th{
    border-bottom: 0px;
  }
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  </style>

</head>

<body>
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <!-- Desenho do cadastro -->
  <br>
  <br>
  <?php
      if($val==1){
       print($msg7);
       }
  ?>
  <br>
  <br>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>
            <a href="parametros.php">
              <i
                class="fas fa-cogs fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Parametros</b>
              </label>
            </a>
          </th>

          <th>
            <a href="grid_usr.php?operacao=ativos">
              <i
                class="fas fa-user fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Usuarios</b>
              </label>
            </a>
          </th>

          <th>
            <a href="grid_empres.php">
              <i
                class="fas fa-building fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Empresa</b>
              </label>
            </a>
          </th>
        </tr>
      </thead>
      <tr>
        <th>
      <a href="valsistema.php">
              <i
                class="fas fa-calendar-alt fa-7x"
                style="color:#696969"
              ></i>

              <br>

              <label> 
                <b style="color:#696969">Validar sistema</b>
              </label>
            </a>
          </th>
     </tr>
    </table>
  </div>
</body>
</html>