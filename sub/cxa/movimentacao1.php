<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$sqlvernivel = "select nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$namesis = $_SESSION["namesis"];
$data = isset($_GET['data']) ? $_GET['data'] : '';
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$caixa = isset($_GET['caixa']) ? $_GET['caixa'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg ="<div class=\"alert alert-danger\"><strong>O caixa já está aberto</strong><br>Esse operação não e possivel.</div>";
$msg1="<div class=\"alert alert-success\" role=\"alert\">Caixa aberto com sucesso!</div>";
if($data==''){
  $data=date("Y-m-d");
}
//Verificar se o caixa está aberto.

if($operacao== 'acessar'){
  $sql="select * from sitcaixa where codigo=$caixa and data='$data' order by id desc limit 1";
  $res=pg_query($conexao,$sql);
  $row=pg_fetch_Assoc($res);
  //Testando se o caixa a ser aberto já tem registro no banco de dados no banco de dados
  if($data == $row['data'] && $caixa == $row['codigo']){
 
  header("Location: abertura.php?alert=1");
  exit;
  }
  if($data != $row['data'] && $caixa != $row['codigo']){
    $ins="insert into sitcaixa (codigo,data,fechado) values ($caixa,'$data','f')";
    $res1=pg_query($conexao,$ins);
    header("Location: abertura.php?alert=2");
   exit;
  }
}
// Consulta de caixa para abertura;
$caixaInicial = "";
$sqlcxa = "select * from formas";
$rescxa = pg_query($conexao,$sqlcxa); 
$htmlselect= "";
while ($row=pg_fetch_assoc($rescxa)){
  $htmlselect=$htmlselect.("<option value=\"".trim($row["id"])."\">".$row["descricao"]."</option>");
}

$sqlgrid="select s.id,s.historico,f.descricao,s.tipo,s.valor From mov_cxa s inner join formas f on (s.forma=f.id) where caixa=$caixa and data='$data' order by id asc";
$resgrid=pg_query($conexao,$sqlgrid);
$exibicao = "";

$sqlentrada="select sum(valor) as entrada from mov_cxa where data='$data' and caixa=$caixa and tipo='E'";
$resentrada = pg_query($conexao,$sqlentrada);
$rowe = pg_fetch_assoc($resentrada);

$sqlsaida="select sum(valor) as saida from mov_cxa where data='$data' and caixa=$caixa and tipo='S'";
$ressaida = pg_query($conexao,$sqlsaida);
$rows = pg_fetch_assoc($ressaida);

$sqlsaldo="select sum(valor) - (select sum(valor) from mov_cxa where data='$data' and caixa=$caixa and tipo='S') as saldo from mov_cxa where data='$data' and caixa=$caixa and tipo='E'";
$ressaldo = pg_query($conexao,$sqlsaldo);
$rowsa = pg_fetch_assoc($ressaldo);
$saldo = "";
$saldo = $rowsa['saldo'];
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Movimentação de Caixa</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>


  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script> 
  
    <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.js"
  ></script> 
  
     <link
    rel="stylesheet"
    href="../../boot/jqueryui/jquery-ui.css"
  /> 

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>


<script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>45 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>

<style>
  body {
    text-align:center;
  }

  .table td, .table th {
    padding: .15rem;
}
  </style>

</head>

<body>
<form  name="suprimento" method="post" action="../../rec/caixajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"../../menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../fat/faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../../logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>
  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  
  <div align="center">
  <?php
  if ($alert == 1){
    echo $msg;
  }  if ($alert == 2){
    echo $msg1;
  }
 // print $data;
  ?>
  </div>
  <input  name="data" type="hidden" value='<?php echo $data; ?>'/>
  <input  name="caixa" type="hidden" value='<?php echo $caixa; ?>'/>
  <input  name="operacao" type="hidden" value="sangria"/>
  <br>
  <h3 align="center">Movimentação do Caixa: <?php echo $caixa ?> </h3>

<table align="left">
<tr>
<td style="color:green;">
 Entradas R$:   
</td> 
<td style="color:green;">
<?php echo $rowe['entrada']; ?>
</td>
</tr> 
<tr>  
<td style="color:red;">
Saidas R$:   
</td> 
<td style="color:red;">
<?php echo $rows['saida']; ?>
</td> 
</tr>    
<tr>  
<td <?php $exibe = $saldo > 0 ? "style=color:green;" : "style=color:red;"; echo $exibe; ?>>
Saldo R$:   
</td> 
<td <?php $exibe = $saldo > 0 ? "style=color:green;" : "style=color:red;"; echo $exibe; ?>>
<?php echo $saldo; ?>
</td> 
</tr>    
</table>    
  <br>
  <div align="center">
  <div class="form-row">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Caixa:</label>&nbsp&nbsp
  <input type="text" readonly="true" style="text-align:center" class="form-control form-control-sm col-md-1" id="caixa"  name="caixa" value="<?php echo $caixa; ?>">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <a href="movimentacao2.php?operacao=lancar&data=<?php echo $data; ?>&caixa=<?php echo $caixa; ?>"><button type="button" class="btn btn-secondary">Lançar Vendas</button></a>
  &nbsp&nbsp
  <a href="caixa.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Data:</label>&nbsp&nbsp
  <input type="date" id="data" readonly="true" name="data" value="<?php echo $data;?>">
  </div>
</div>

<table class="table table-hover">
    <thead>
      <tr>
        <th>Nº</th>
        <th>Historico</th>
        <th>Forma</th>
        <th>Tipo</th>
        <th>Valor</th>
      </tr>
    </thead>
    <tbody>
    <?php
   while ($row=pg_fetch_assoc($resgrid)){
    $exibicao="<tr>".
    "<td>".$row["id"]."</td>".
    "<td>".$row["historico"]."</td>".
    "<td>".$row["descricao"]."</td>".
    "<td>".$row["tipo"]."</td>".
    "<td>".$row["valor"]."</td>"."</tr>";
    print("$exibicao");
  }
  ?>
    </tbody>
  </table>
</form>
</body>
</html>
