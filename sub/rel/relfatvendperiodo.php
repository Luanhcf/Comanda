<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$datai = isset($_POST['datai']) ? $_POST['datai'] : date('Y-m-d');
$dataf = isset($_POST['dataf']) ? $_POST['dataf'] : date('Y-m-d');

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Relatorios</title>

  <meta charset="utf-8"></meta>


  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table thead th{
    border-bottom: 0px;
  }
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  </style>
  <script>
      $( function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker" ).val("<?php echo $datai; ?>");
      } );
    </script>
    <script>
      $( function() {
        $( "#datepicker2" ).datepicker();
        $( "#datepicker2" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker2" ).val("<?php echo $dataf; ?>");
      } );
    </script>
</head>
<body>
<form  name="Vendas_no_Periodo" method="post" target="_blank" action="relfatvendperiodoimp.php"  enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Faturamento
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <br>
<h3> Vendas no periodo </h3>
<br>
<a href="relfat.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
<button type="submit" class="btn btn-secondary">Imprimir</button></a>
<br><br>
<div align="Center">
  <label><b>Data inicial</b></label>
  <input class="form-control form-control-sm col-md-2" name ="datai" id="datepicker" size="30" type="text" value="<?php echo $datai; ?>">
  <label><b>Data Final</b></label>
  <input class="form-control form-control-sm col-md-2" name ="dataf" id="datepicker2" type="text"  value="<?php echo $dataf; ?>">
  <br>  
</div>
  <table align="center">
    <tr>
      <td></td>
   </tr> 
</table>
</body>
</html>