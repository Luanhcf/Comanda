<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$errocpf = isset($_GET['errocpf']) ? $_GET['errocpf'] : '0';
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
$id     =    ''; 
$pessoa   =  '';
$datacad   = date("d-m-Y");
$nome     =  ''; 
$apelido  =  '';
$endereco =  '';
$bairro   =  '';
$cidade   =  '';
$cpf      =  ''; 
$cnpj     =  '';
$ie       =  '';
$email    =  '';
$telefone =  '';
$celular  =  '';
$status   =  '';
$obs      =  '';
$descricao = 'Cadastrar Cliente';
}else{
$id = isset($_GET['id']) ? $_GET['id'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from clientes where id = $id";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);

//Variaveis retornando do banco de dados
$pessoa = trim($row['pessoa']); //
$nome = trim($row['nome']);
$datacad = trim($row['datacad']);
$apelido = trim($row['apelido']);
$endereco = trim($row['endereco']);
$bairro = trim($row['bairro']);
$cidade = trim($row['cidade']);
$cpf = trim($row['cpf']);
$cnpj = trim($row['cnpj']);
$ie = trim($row['ie']);
$email = trim($row['email']);
$telefone = trim($row['telefone']);
$celular = trim($row['celular']);
$status = trim($row['status']);
$obs = trim($row['obs']);
$descricao = 'Editar Cliente';
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Inserir Cliente</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

</head>

<body>
<form  name="cad_cli" method="post" action="../../rec/cliajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>
      </ul>
    </div>
  </nav>
  <br>
  <h3>&nbsp&nbsp<?php echo $descricao; ?></h3>
  <br>
  <div class="col-md-10">
  <div class="form-row">
    <div class="form-group col-md-1">
    <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
      <label>Codigo</label>
      <input type="text" class="form-control form-control-sm" id="id" value="<?php echo $id; ?>" readonly="true" name="id">
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label for="pessoa">Pessoa</label>
      <select id="pessoa" class="form-control form-control-sm" onchange="mudacampos();" name="pessoa">
        <option value="f" <?php echo $pessoa == 'f' ? "selected" : ""; ?>>Fisica</option>
        <option value="j" <?php echo $pessoa == 'j' ? "selected" : ""; ?>>Juridica</option>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label for="pessoa">Status</label>
      <select id="status" class="form-control form-control-sm" value="<?php echo $status; ?>" name="status">
        <option value="t" <?php echo $status == 't' ? "selected" : ""; ?>>Ativo</option>
        <option value="f" <?php echo $status == 'f' ? "selected" : ""; ?>>Inativo</option>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label>Data Cadastro</label>
      <input type="text" class="form-control form-control-sm" id="datacad" readonly="true" value="<?php echo $datacad; ?>" name="datacad">
    </div>  
  </div>
  <div class="form-row">
  <div class="form-group col-md-6">
    <label>Nome</label>
    <input type="text" class="form-control form-control-sm" id="nome"  name="nome" value="<?php echo $nome; ?>" maxlength="50" >
  </div>
  <div class="form-group col-md-4">
      <label>Apelido</label>
      <input type="text" class="form-control form-control-sm" id="apelido" value="<?php echo $apelido; ?>" name="apelido" maxlength="50">
    </div>
</div>
<div class="form-row">
  <div class="form-group col-md-6">
    <label>Endereço</label>
    <input type="text" class="form-control form-control-sm" id="endereco" name="endereco" value="<?php echo $endereco; ?>" maxlength="50">
  </div>
  <div class="form-group col-md-2">
    <label>Bairro</label>
    <input type="text" class="form-control form-control-sm" id="bairro" name="bairro" value="<?php echo $bairro; ?>" maxlength="25">
  </div>
  <div class="form-group col-md-2">
    <label>Cidade</label>
    <input type="text" class="form-control form-control-sm" id="cidade" name="cidade" value="<?php echo $cidade; ?>" maxlength="25" >
  </div>
</div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label>CPF</label>
      <input type="text" class="form-control form-control-sm" id="cpf" name="cpf" value="<?php echo $cpf; ?>" onKeyPress="formata_mascara(this,'###.###.###-##','#')" maxlength="14">
    </div>
    <div class="form-group col-md-3">
      <label id="lcnpj">CNPJ</label>
      <input type="text" class="form-control form-control-sm" id="cnpj" name="cnpj" value="<?php echo $cnpj; ?>" onKeyPress="formata_mascara(this,'##.###.###/####-##','#')" maxlength="18">
    </div>
    <div class="form-group col-md-3">
      <label id="lie">I.E</label>
      <input type="text" class="form-control form-control-sm" id="ie" name="ie" value="<?php echo $ie; ?>" maxlength="20">
    </div> 
  </div>
  <div class="form-row">
  <div class="form-group col-md-4">
      <label>E-mail</label>
      <input type="text" class="form-control form-control-sm" id="email" name="email" value="<?php echo $email; ?>" maxlength="50">
    </div> 
    <div class="form-group col-md-3">
      <label>Telefone</label>
      <input type="text" class="form-control form-control-sm" id="telefone" name="telefone" value="<?php echo $telefone; ?>" maxlength="13" onKeyPress="formata_mascara(this,'(##)####-####','#')" placeholder="(86)3333-3333">
    </div>
    <div class="form-group col-md-3">
      <label>Celular</label>
      <input type="text" class="form-control form-control-sm" id="celular" name="celular" value="<?php echo $celular; ?>" maxlength="14" onKeyPress="formata_mascara(this,'(##)#####-####','#')" placeholder="(86)99999-9999">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-10">
    <label>Observação</label>
    <textarea class="form-control form-control-sm" id="obs" name="obs" rows="3" maxlength="1000"><?php echo $obs; ?> </textarea>
  </div>
</div>
<button type="submit" class="btn btn-secondary">Gravar</button>
<a href="cad_grid.php?operacao=ativos"><button type="button" class="btn btn-secondary">Voltar</button></a>
  </form>
</body>
</html>