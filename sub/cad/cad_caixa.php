<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$descricao='';
$titulo ='';
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
$id      =    ''; 
$descr   =  '';
$descricao= 'Cadastro de Mesas';
$titulo = 'Cadastrar Mesas';
}else{
$id = isset($_GET['id']) ? $_GET['id'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from caixa where id = $id";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);

//Variaveis retornando do banco de dados
$descr = trim($row['descricao']); //
$descricao= 'Edição Caixas';
$titulo = 'Editar caixas';
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $titulo; ?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

</head>

<body>
<form  name="cad_caixa" method="post" action="../../rec/caixajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
      ><?php echo $namesis; ?> </a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp<?php echo $descricao; ?></h3>
  <br>
  <div class="col-md-10">
  <div class="form-row">
    <div class="form-group col-md-1">
    <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
      <label>Codigo</label>
      <input type="text" class="form-control form-control-sm" id="id" value="<?php echo $id; ?>" readonly="true" name="id">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-4">
    <label>Descrição</label>
    <input type="text" class="form-control form-control-sm" id="descr"  name="descr" value="<?php echo $descr; ?>" maxlength="50" >
  </div>
</div>
<br>
<button type="submit" class="btn btn-secondary">Gravar</button>
<a href="caixa_grid.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
</div>
  </form>
</body>
</html>