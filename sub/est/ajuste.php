<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$sqlprod = "select * from produtos where status='t' and tipo='P' order by descricao";
$resprod = pg_query($conexao,$sqlprod); 
$htmlselect2= "";
//$htmlselect1=$htmlselect1.("<option value=\"%\">Todos os Status</option>");
while ($row=pg_fetch_assoc($resprod)){

  $htmlselect2=$htmlselect2.("<option value=\"".trim($row["id"])."\" >".$row["descricao"]."</option>");
}


?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Ajustes/Acerto</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table thead th{
    border-bottom: 0px;
    align:center;
  }
  .table td, .table th{
    border-top: 0px;
    align:center;
    padding:0rem;
  }
  body {
    text-align:center;
  }
  </style>
  <script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>

</head>

<body>
<form  name="ajuste" method="post" action="../../rec/ajusteajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp Ajustes/Acertos de Estoque</h3>
  <br>
  <button type="submit" class="btn btn-secondary" onclick="if(!confirm('Deseja realmente realizar o Ajuste/Acerto?'))return false;">Salvar</button>
<a href="estoque.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
<br><br>
    <table class="table">
      <thead align="center">
        <tr>
          <th>
          <div class="form-group col-md-2">
          <label>Motivo de Ajuste</label>
          <select id="motivo" class="form-control form-control-sm" name="motivo">
          <option value="E">Entrada</option>
          <option value="S">Saida</option>
      </select>
        </div>       
          </th>
        </tr>
        <tr>
          <th>
          <div class="form-group col-md-3">
          <label>Selecionar Produto</label>
          <select id="produto" class="form-control form-control-sm" name="produto">
           <?php
           echo $htmlselect2;
           ?>
      </select>
        </div>       
          </th>
        </tr>
        <tr>
          <th>
          <div class="form-group col-md-1">
          <label>Quantidade</label>
          <input type="text" class="form-control form-control-sm" id="quantidade" placeholder="0" required name="quantidade" onkeypress='return SomenteNumero(event)'>
        </div>       
          </th>
        </tr>
        <tr>
          <th>
          <div class="form-group col-md-5">
          <label>Motivo</label>
          <input type="text" class="form-control form-control-sm" id="obs" name="obs" placeholder="Ex: Entrada de mercadorias">
        </div>       
          </th>
        </tr>
      </thead>
    </table>
</form>
</body>
</html>