<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';

//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
  $nome       = '';
  $fantasia   = '';
  $endereco   = '';
  $bairro     = '';
  $cidade     = '';
  $cnpj       = '';
  $insc       = '';
  $email      = '';
  $celular    = '';
  $telefone   = '';
  $nome1 = 'Cadastro de Empresa';
  $desabilitado = '';
}
else{
$id = isset($_GET['id']) ? $_GET['id'] : '';
//Consulta no banco de dados edição.
$sql1= "select * from empresa where id = $id";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);
//Variaveis retornando do banco de dados
$nome     = trim($row['nome']);
$fantasia = trim($row['fantasia']);
$endereco = trim($row['endereco']);
$bairro   = trim($row['bairro']);
$cidade   = trim($row['cidade']);
$cnpj     = trim($row['cnpj']);
$insc     = trim($row['insc']);
$email    = trim($row['email']);
$celular  = trim($row['celular']);
$telefone = trim($row['telefone']);
$nome1    = 'Alteração de Empresa';
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $nome1;?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_empres.js"
  ></script>
</head>

<body>
<form  name="cad_empres" method="post" action="../../rec/empresajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp<?php echo $nome1; ?></h3>
  <br>
  <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
  <div class="col-md-10">
  <div class="form-row">
  <div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control form-control-sm" id="nome" required name="nome" value="<?php echo $nome; ?>" maxlength="50" >
  </div>
  <div class="form-group col-md-3">
      <label>Fantasia</label>
      <input type="text" class="form-control form-control-sm" required id="fantasia" value="<?php echo $fantasia; ?>" name="fantasia" maxlength="50">
    </div>
</div>
<div class="form-row">
  <div class="form-group col-md-3">
    <label>Endereço</label>
    <input type="text" class="form-control form-control-sm" required id="endereco" name="endereco" value="<?php echo $endereco; ?>" maxlength="50">
  </div>
  <div class="form-group col-md-2">
    <label>Bairro</label>
    <input type="text" class="form-control form-control-sm" id="bairro" name="bairro" value="<?php echo $bairro; ?>" maxlength="25">
  </div>
  <div class="form-group col-md-2">
    <label>Cidade</label>
    <input type="text" class="form-control form-control-sm" id="cidade" name="cidade" value="<?php echo $cidade; ?>" maxlength="25" >
  </div>
</div>
  <div class="form-row">
    <div class="form-group col-md-1.4">
      <label>CNPJ</label>
      <input type="text" class="form-control form-control-sm" required id="cnpj" name="cnpj" value="<?php echo $cnpj; ?>" onKeyPress="formata_mascara(this,'##.###.###/####-##','#')" maxlength="18">
    </div>
    <div class="form-group col-md-1.4">
      <label>I.E</label>
      <input type="text" class="form-control form-control-sm" id="insc" name="insc" value="<?php echo $insc; ?>" maxlength="14">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-3">
      <label>E-mail</label>
      <input type="text" class="form-control form-control-sm" id="email" name="email" value="<?php echo $email; ?>" maxlength="50">
    </div>
    <div class="form-group col-md-2">
      <label>Celular</label>
      <input type="text" class="form-control form-control-sm" id="celular" name="celular" value="<?php echo $celular; ?>" maxlength="14" onKeyPress="formata_mascara(this,'(##)#####-####','#')" placeholder="(86)99999-9999">
    </div>
  <!--  -->
  <div class="form-group col-md-2">
      <label>Telefone</label>
      <input type="text" class="form-control form-control-sm" id="telefone" name="telefone" value="<?php echo $telefone; ?>" maxlength="13" onKeyPress="formata_mascara(this,'(##)####-####','#')" placeholder="(86)3333-3333">
    </div> 
  </div>
        <button class="btn btn-secondary"  type="submit">Salvar</button>
        <button class="btn btn-secondary" type="reset">Limpar</button>
        <a href="grid_empres.php"><button class="btn btn-secondary" type="button">Voltar</button></a>
     </div>
  </form>
</body>
</html>