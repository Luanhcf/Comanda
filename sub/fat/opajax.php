<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$operacoes = isset($_GET['operacoes']) ? $_GET['operacoes'] : '';
$operacoes   = isset($_POST['operacoes']) ? $_POST['operacoes'] : '';
$idmesa   = isset($_POST['idmesa']) ? $_POST['idmesa'] : '';
$detalhe   = isset($_POST['detalhe']) ? $_POST['detalhe'] : '';
$valor   = isset($_POST['valor']) ? $_POST['valor'] : '';
$divisor   = (int)(isset($_POST['divisor']) ? $_POST['divisor'] : '0'); //variaveis do fracionamento
$produto   = isset($_POST['produto']) ? $_POST['produto'] : ''; //variaveis do fracionamento


if($operacoes == "fracionar"){
$produto = explode(":",$produto);	
$up="update mov_sai set quant=quant/$divisor where id=$produto[0]";
$coz="update pedidos set quant=quant/$divisor where id=$produto[1]";
$upres= pg_query($conexao,$up);
$upcoz= pg_query($conexao,$coz);
header("Location: lanc_fat.php?idmesa=$idmesa");
}

if($operacoes == "cozinha"){
for($cont = 1;$cont < 1000; $cont ++){

	$verifica   = isset($_POST["verifica$cont"]) ? $_POST["verifica$cont"] : '';
	$idpedido   = isset($_POST["idpedido$cont"]) ? $_POST["idpedido$cont"] : '';
	if($verifica == 'on'){
		$sqlup="update pedidos set finaliza='t',dataf=current_timestamp where id=$idpedido";
		$exec=pg_query($conexao,$sqlup);

	}
}
header("Location: cozinha.php");
exit;
}

if($operacoes == "cancelar"){
	for($cont = 1;$cont < 1000; $cont ++){
		$verifica   = isset($_POST["verifica$cont"]) ? $_POST["verifica$cont"] : '';
		$idpedido   = isset($_POST["idpedido$cont"]) ? $_POST["idpedido$cont"] : '';
		if($verifica == 'on'){
			$sqlup="delete from pedidos  where id=$idpedido";
			$exec=pg_query($conexao,$sqlup);
	
		}
	}
	header("Location: cozinha.php");
	exit;
	}

if($operacoes == "desconto"){
	$titulo='Aplicar desconto';
	$sql="select me.atend,me.id,mo.func,mo.cliente From mesas me inner join mov_sai mo on (me.atend=mo.atend) where me.id=$idmesa and me.ocupada='t' and mo.total > 0 limit 1";
	$res=pg_query($conexao,$sql);
	$row=pg_fetch_assoc($res);
	$inserir="insert into mov_sai (quant,id_mesa,atend,detalhe,func,total,cliente,data) values 
	(1,".$row['id'].",".$row['atend'].",upper('$detalhe'),".$row['func'].",-$valor,".$row['cliente'].",current_timestamp)";
    $res1=pg_query($conexao,$inserir);
	header("Location: grid_fat_mes.php?operacoes=desconto&idmesa=$idmesa");
    //exit;
	}
	
	if($operacoes == "acrescimo"){
	$titulo='Aplicar acrescimo';
	$sql="";
	$sql="select me.atend,me.id,mo.func,mo.cliente From mesas me inner join mov_sai mo on (me.atend=mo.atend) where me.id=$idmesa and me.ocupada='t' and mo.total > 0 limit 1";
	$res=pg_query($conexao,$sql);
	$row=pg_fetch_assoc($res);
	$inserir="insert into mov_sai (quant,id_mesa,atend,detalhe,func,total,cliente,data) values 
	(1,".$row['id'].",".$row['atend'].",upper('$detalhe'),".$row['func'].",$valor,".$row['cliente'].",current_timestamp)";
	$res1=pg_query($conexao,$inserir);
	header("Location: grid_fat_mes.php?operacoes=acrescimo&idmesa=$idmesa");
  // exit;
	}
	
?>