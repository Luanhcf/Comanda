<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$erro = isset($_GET['erro']) ? $_GET['erro'] : '';
$msgerror='';
$msgerror="<div class=\"alert alert-danger\"><strong>Nivel de usuario não corresponde com função do funcionario</strong> Verifique os dados.
</div>";
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$desabilitado ="";
$nome="";

if($erro == 1){
  $usuario = isset($_GET['usuario']) ? $_GET['usuario'] : ''; //
  $nivel   = isset($_GET['nivel']) ? $_GET['nivel'] : '';
  $func    = isset($_GET['func']) ? $_GET['func'] : '';
  $status = isset($_GET['status']) ? $_GET['status'] : '';
  $bloquea_op = isset($_GET['bloquea_op']) ? $_GET['bloquea_op'] : '';
  }
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
  $usuario = '';
  $senha   = '';
  $nivel   = '';
  $func    = '';
  $status  = '';
  $bloquea_op  = '';
  $nome = 'Cadastro de Usuário';
  $desabilitado = '';
}
else{
$usuario = isset($_GET['usuario']) ? $_GET['usuario'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from usuarios where usuario = '$usuario'";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);
//Variaveis retornando do banco de dados
//$usuario = trim($row['descricao']); //
$usuario = trim($row['usuario']);
$senha = '';
$nivel = trim($row['nivel']);
$func = trim($row['func']);
$status = trim($row['status']);
$bloquea_op = trim($row['bloquea_op']);
$desabilitado = "readonly";
$nome = 'Alteração de Usuário';
}
//Consulta funcionario
$sqlfun = "select * from funcionarios where status='t' order by nome";
$resfun = pg_query($conexao,$sqlfun); 
$opfun= "";
//$htmlselect1=$htmlselect1.("<option value=\"%\">Todos os Status</option>");
while ($row=pg_fetch_assoc($resfun)){

  $opfun=$opfun.("<option value=\"".trim($row["id"])."\"".(trim($row["id"]) == trim($func) ? "selected" : "").">".$row["nome"]."</option>");
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $nome;?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/valida_user.js"
  ></script> 
</head>

<body>
<form  name="cad_usr" method="post" action="../../rec/userajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <div class="container">
      <div class="row">
       <div class="col-sm-8 contact-form">
         <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
         <h3><?php echo $nome;?></h3>
         <br>
         <div class="col-xs-4 col-md-8 form-group">
          <label>Usuario</label>
          <div class="controls">
           <input class="form-control" id="usuario" value="<?php echo $usuario; ?>" name="usuario" <?php echo $desabilitado;?> placeholder="Usuario" required autofocus type="text">
         </div>
       </div>
       <div class="col-xs-4 col-md-8 form-group">
        <label>Senha</label>
        <div class="controls">
         <input class="form-control" id="senha" type="password" value="<?php echo $senha; ?>" name="senha" placeholder="Senha" required autofocus type="text">
       </div>
       <br> 
       <label><b>Nivel do usuario</b></label>  
       <br>      
       <select name="nivel" id="nivel" onchange="mudacampos();"  class="form-control">
         <option value="Administrador" <?php echo trim($nivel) == "Administrador" ? "selected" : "" ?>>Administrador</option>
         <option value="Garcom" <?php echo trim($nivel) == "Garcom" ? "selected" : "" ?>>Garcom</option>
         <option value="Cozinha" <?php echo trim($nivel) == "Cozinha" ? "selected" : "" ?>>Cozinha</option>
         <option value="Caixa" <?php echo trim($nivel) == "Caixa" ? "selected" : "" ?>>Caixa</option>
       </select>
       <br> 
      <div class="form-row">
        <div class="form-group">
        <label id="func1"><b>Funcionario</b></label>  
       <br>      
      <select name="func" id="func" class="form-control">
        <?php
        echo $opfun;
        ?>
       </select> 
       </div>
       &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
       <div class="form-group">
       <label><b>Status</b></label>
       <br>      
       <select name="status" id="status" class="form-control">
         <option  value="t" <?php echo trim($status) == "t" ? "selected" : "" ?>>Ativo</option>
         <option  value="f" <?php echo trim($status) == "f" ? "selected" : "" ?>>Inativo</option>
         </select>
         </div>
         <div class="form-group">
         <label id="bloqlabel"><b>Bloquear ações operação</b></label>
         <select name="bloquea_op" id="bloquea_op" class="form-control">
         <option  value="t" <?php echo trim($bloquea_op) == "t" ? "selected" : "" ?>>Sim</option>
         <option  value="f" <?php echo trim($bloquea_op) == "f" ? "selected" : "" ?>>Não</option>
         </select>
         </div>
         <?php
    if($erro == 1){
      echo $msgerror;
            } 
       ?>
         </div>
         </div>
        <div class="col-xs-12 col-md-12 form-group">
        <button class="btn btn-secondary"  type="submit">Salvar</button>
        <button class="btn btn-secondary" type="reset">Limpar</button>
        <a href="grid_usr.php"><button class="btn btn-secondary" type="button">Voltar</button></a>
     </div>
  </form>
</body>
</html>