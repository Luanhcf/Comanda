<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$sqlvernivel = "select nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$data = isset($_GET['data']) ? $_GET['data'] : '';
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
$caixa = isset($_GET['caixa']) ? $_GET['caixa'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg ="<div class=\"alert alert-danger\"><strong>O caixa já está aberto</strong><br>Esse operação não e possivel.</div>";
$msg1="<div class=\"alert alert-success\" role=\"alert\">Caixa aberto com sucesso!</div>";
if($data==''){
  $data=date("Y-m-d");
}
//Verificar se o caixa está aberto.

if($operacao== 'acessar'){
  $sql="select * from sitcaixa where codigo=$caixa and data='$data' order by id desc limit 1";
  $res=pg_query($conexao,$sql);
  $row=pg_fetch_Assoc($res);
  //Testando se o caixa a ser aberto já tem registro no banco de dados no banco de dados
  if($data == $row['data'] && $caixa == $row['codigo']){
 
  header("Location: abertura.php?alert=1");
  exit;
  }
  if($data != $row['data'] && $caixa != $row['codigo']){
    $ins="insert into sitcaixa (codigo,data,fechado) values ($caixa,'$data','f')";
    $res1=pg_query($conexao,$ins);
    header("Location: abertura.php?alert=2");
   exit;
  }
}
// Consulta de caixa para abertura;
$caixaInicial = "";
$sqlcxa = "select * from formas";
$rescxa = pg_query($conexao,$sqlcxa); 
$htmlselect= "";
while ($row=pg_fetch_assoc($rescxa)){
  $htmlselect=$htmlselect.("<option value=\"".trim($row["id"])."\">".$row["descricao"]."</option>");
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Suprimento - Operação</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>


  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script> 
  
    <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.js"
  ></script> 
  
     <link
    rel="stylesheet"
    href="../../boot/jqueryui/jquery-ui.css"
  /> 


  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>


  <style>
  .table thead th{
    border-bottom: 0px;
  }
  .table td, .table th{
    border-top: 0px;
  }
  </style>

<script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>45 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>

</head>

<body>
<form  name="suprimento" method="post" action="../../rec/caixajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"../../menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../fat/faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br><br>
  <div align="center">
  <?php
  if ($alert == 1){
    echo $msg;
  }  if ($alert == 2){
    echo $msg1;
  }
 // print $data;
  ?>
  </div>
  <input  name="data" type="hidden" value='<?php echo $data; ?>'/>
  <input  name="caixa" type="hidden" value='<?php echo $caixa; ?>'/>
  <input  name="operacao" type="hidden" value="suprimento"/>
  <br>
  <h3 align="center">Suprimento no caixa: <?php echo $caixa ?> </h3>
  <br>
  <div align="center">
  <div class="form-row">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Caixa:</label>&nbsp&nbsp
  <input type="text" readonly="true" style="text-align:center" class="form-control form-control-sm col-md-1" id="caixa"  name="caixa" value="<?php echo $caixa; ?>">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Data:</label>&nbsp&nbsp
  <input type="date" id="data" readonly="true" name="data" value="<?php echo $data;?>">
  </div>
  <br>
  <div class="form-row">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Forma de Pagamento:</label>&nbsp&nbsp
  <select id="forma" class="form-control form-control-sm col-md-1" name="forma">
    <?php
    echo $htmlselect;
    ?>
    </select>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <label>Valor R$:</label>&nbsp&nbsp
    <input type="text"  class="form-control form-control-sm col-md-1" id="valor" onkeypress='return SomenteNumero(event)' maxlength="13" name="valor">
  </div>
  <br>
  <div class="form-row">
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
  <label>Historico:</label>&nbsp&nbsp&nbsp
  <input type="text"  class="form-control form-control-sm col-md-5" id="historico"  maxlength="60" name="historico">
  </div>
  <br><br>
  <button type="submit" class="btn btn-secondary">Gravar</button>
  <a href="caixa.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
</div>
</form>
</body>
</html>
