<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$sqlvernivel = "select nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$sql="select pe.id,
pe.id_mesa as mesa,
pe.atend as atendimento,
pro.descricao,
fu.apelido,
quant as quantidade,
to_char(data,'dd-mm-yyyy hh24:mi') as data,
to_char(dataf,'dd-mm-yyyy hh24:mi') as dataf,
to_char((case when dataf is null then current_timestamp else dataf end)-data,'hh24:mi') as tempo, 
case when finaliza='t' then 'Atendido'
     when finaliza='f' then 'Aguardando' else 'Cancelado' end as atendido
From pedidos pe inner join produtos pro on (pe.codprod=pro.id) inner join funcionarios fu on (pe.func=fu.id)
order by dataf desc,pe.id limit 100";
$select="";
$res=pg_query($conexao,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Consulta Mesas</title>
  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  body {
    text-align:center;
  }
  </style>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
 <script>
      $( function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker" ).val("<?php echo $datai; ?>");
      } );
    </script>
    <script>
      $( function() {
        $( "#datepicker2" ).datepicker();
        $( "#datepicker2" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker2" ).val("<?php echo $dataf; ?>");
      } );
    </script>
</head>

<body>
<form  name="cozinha" method="post" action="opajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"../../menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../../logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/cxa/caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <!-- Desenho do cadastro -->
  <div class="container">
  <br>
  <h2>Pedidos Cozinha</h2> 
  <input type="hidden" name="operacoes" id="operacoes" value="cozinha">
  <button type="submit" onclick="$('#operacoes').val('cancelar');" class="btn btn-secondary">Excluir</button>
  <button type="submit" class="btn btn-secondary">Liberar Pedidos</button>
  <a href="cozinha.php"><button type="button" class="btn btn-secondary">Atualizar Pagina</button></a>
  <a href="faturamento.php"><button type="button" class="btn btn-secondary">Voltar</button></a>
  <br><br>
  <div align="left">
  <input class="form-control form-control-sm col-md-4" id="myInput" type="text" placeholder="Buscar .."> 
</div>
  <p align="center">
</p>
  <table class="table table-sm">
    <thead>
      <tr>
        <th>#</th>
        <th>id</th>
        <th>Mesa</th>
        <th>Nº Atendimento</th>
        <th>Descricao</th>
        <th>Garçom</th>
        <th>QTD</th>
        <th>Dt/Hor Ini</th>
        <th>Dt/Hr Fin</th>
        <th>Duração</th>
        <th>Situação</th>
        <th>Imprimir</th>
      </tr>
    </thead>
    <tbody id="myTable">
    <?php
    $cont= 0;
   while ($row=pg_fetch_assoc($res)){
     $cont = $cont +1;
    $select="<tr>
    <td>".(trim($row["atendido"]) == "Aguardando" ? "<div class=\"checkbox\"><input type=\"checkbox\" name=\"verifica$cont\"> </div>" : "<div class=\"checkbox disabled\"> <input type=\"checkbox\" name=\"verifica$cont\" disabled> </div>")."<input type=\"hidden\" name=\"idpedido$cont\" value=\"$row[id]\"></td>
    <td>".$row['id']."</td>
    <td>".$row['mesa']."</td>
    <td>".$row['atendimento']."</td>
    <td>".$row['descricao']."</td>
    <td>".$row['apelido']."</td>
    <td>".$row['quantidade']."</td>
    <td>".$row['data']."</td>
    <td>".$row['dataf']."</td>
    <td>".$row['tempo']."</td>
    <td><b><font color=\"".(trim($row["atendido"]) == "Aguardando" || trim($row["atendido"]) == "Cancelado"  ? "red" : "green")."\">".$row['atendido']."</font></b></td>
    <td><a href=\"imprimeped.php?id=".$row['id']."\" target=\"_blank\"><i class=\"fas fa-print\"></i></a></td>
    </tr>";
    print("$select");
  }
  ?>
  </form>
</body>
</html>