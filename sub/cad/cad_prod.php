<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';

if($operacao=="novo"){
  $id           =  ''; 
  $class        =  '';
  $status       =  '';
  $tipo         =  '';
  $datacad      = date("d-m-Y");
  $descricao    =  ''; 
  $complemento  =  '';
  $codbar       =  '';
  $und          =  '';
  $embalagem    =  '';
  $marca        =  ''; 
  $pr_compra    =  '';
  $pr_custo     =  '';
  $pr_venda     =  '';
  $estoque      =  '';
  $fornecedor   =  '';
  $imagem       =  '';
  $receita      =  '';
  $texto = 'Cadastro de Produtos';
  }else{
  $id = isset($_GET['id']) ? $_GET['id'] : ''; //
  //Consulta no banco de dados edição.
  $sql1= "select * from produtos where id = $id";
  $ressql=pg_query($conexao,$sql1);
  $row=pg_fetch_assoc($ressql);
  //Variaveis retornando do banco de dados
  $class = trim($row['class']); //
  $status = trim($row['status']);
  $datacad = trim($row['datacad']);
  $descricao = trim($row['descricao']);
  $complemento = trim($row['complemento']);
  $codbar = trim($row['codbar']);
  $und = trim($row['und']);
  $embalagem = trim($row['embalagem']);
  $marca = trim($row['marca']);
  $pr_compra = trim($row['pr_compra']);
  $pr_custo = trim($row['pr_custo']);
  $tipo = trim($row['tipo']);
  $pr_venda = trim($row['pr_venda']);
  $estoque = trim($row['estoque']);
  $fornecedor = trim($row['fornecedor']);
  $imagem = trim($row['imagem']);
  $receita = trim($row['receita']);
  $texto = 'Editar Produtos';
  }

$sqlforn = "select * from fornecedores order by fantasia";
$resforn = pg_query($conexao,$sqlforn); 
$htmlselect2= "";
//$htmlselect1=$htmlselect1.("<option value=\"%\">Todos os Status</option>");
while ($row=pg_fetch_assoc($resforn)){

  $htmlselect2=$htmlselect2.("<option value=\"".trim($row["id"])."\" ".(trim($row["id"]) == trim($fornecedor) ? "selected" : "").">".$row["fantasia"]."</option>");
}

// SQL CONSULTA classificacao
$sqlsit = "select * from classificacao order by descricao";
$ressit = pg_query($conexao,$sqlsit); 
$htmlselect1= "";
//$seleciona = "selected";
while ($row=pg_fetch_assoc($ressit)){

  $htmlselect1=$htmlselect1.("<option value=\"".trim($row["id"])."\" ".(trim($row["id"]) == trim($class) ? "selected" : "")."> ".$row["descricao"]."</option>");
}

//Teste para verificar se o cadastro e novo


?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $texto; ?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

  <script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>45 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>

</head>

<body>
<form  name="cad_prod" method="post" action="../../rec/proajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp<?php echo $texto; ?></h3>
  <br>
  <div class="col-md-10">
  <div class="form-row">
    <div class="form-group col-md-1">
    <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
      <label>Codigo</label>
      <input type="text" class="form-control form-control-sm" id="id" value="<?php echo $id; ?>" readonly="true" name="id">
    </div>
    &nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label for="class">Classificação</label>
      <select id="class" class="form-control form-control-sm" required name="class">
       <?php
         echo $htmlselect1;
       ?>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label for="status">Status</label>
      <select id="status" class="form-control form-control-sm" value="<?php echo $status; ?>" name="status">
        <option value="t" <?php echo $status == 't' ? "selected" : ""; ?>>Ativo</option>
        <option value="f" <?php echo $status == 'f' ? "selected" : ""; ?>>Inativo</option>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label for="tipo">Tipo Produto</label>
      <select id="tipo" class="form-control form-control-sm" value="<?php echo $tipo; ?>" name="tipo">
        <option value="P" <?php echo $tipo == 'P' ? "selected" : ""; ?>>Produto - Controla estoque</option>
        <option value="N" <?php echo $tipo == 'N' ? "selected" : ""; ?>>Produto - Não controla estoque</option>
      </select>
    </div>
    &nbsp&nbsp&nbsp&nbsp&nbsp
    <div class="form-group col-md-2">
      <label>Data Cadastro</label>
      <input type="text" class="form-control form-control-sm" id="datacad" readonly="true" value="<?php echo $datacad; ?>" name="datacad">
    </div>  
  </div>
  <div class="form-row">
  <div class="form-group col-md-6">
    <label>Descrição</label>
    <input type="text" class="form-control form-control-sm" id="descricao"  name="descricao" value="<?php echo $descricao; ?>" maxlength="50" >
  </div>
  <div class="form-group col-md-4">
      <label>Complemento</label>
      <input type="text" class="form-control form-control-sm" id="complemento" value="<?php echo $complemento; ?>" name="complemento" maxlength="50">
    </div>
</div>
<div class="form-row">
  <div class="form-group col-md-2">
    <label>Cod.Barras</label>
    <input type="numeric" class="form-control form-control-sm" id="codbar" name="codbar" value="<?php echo $codbar; ?>" maxlength="15" onkeypress='return SomenteNumero(event)'>
  </div>
  <div class="form-group col-md-2">
    <label>Unidade de Venda</label>
    <input type="text" class="form-control form-control-sm" id="und" name="und" placeholder="UND" value="<?php echo $und; ?>" maxlength="3">
  </div>
  <div class="form-group col-md-2">
    <label>Embalagem de compra</label>
    <input type="text" class="form-control form-control-sm" id="embalagem" placeholder="CAIXA" name="embalagem" value="<?php echo $embalagem; ?>" maxlength="15">
  </div>
  <div class="form-group col-md-2">
    <label>Marca</label>
    <input type="text" class="form-control form-control-sm" id="marca" name="marca" value="<?php echo $marca; ?>" maxlength="15">
  </div>
  <!--<div class="form-group col-md-5">
      <label>Foto</label>
      <input id="imagem" type="file" class="form-control" name="imagem" aria-label="Username" aria-describedby="sizing-addon1" maxlength="40">
    </div> -->
</div>
  <div class="form-row">
  <div class="form-group col-md-2">
      <label>Preço de compra</label>
      <input type="text" class="form-control form-control-sm" id="pr_compra" name="pr_compra" value="<?php echo $pr_compra; ?>" maxlength="5" onkeypress='return SomenteNumero(event)' onKeyPress="formata_mascara(this,'#.###','#')">
    </div> 
    <div class="form-group col-md-2">
      <label>Preço de Custo</label>
      <input type="text" class="form-control form-control-sm" id="pr_custo" name="pr_custo" value="<?php echo $pr_custo; ?>" maxlength="5" onkeypress='return SomenteNumero(event)' onKeyPress="formata_mascara(this,'#.###','#')">
    </div>
    <div class="form-group col-md-2">
      <label>Preço de Venda</label>
      <input type="text" class="form-control form-control-sm" id="pr_venda" name="pr_venda" value="<?php echo $pr_venda; ?>" maxlength="5" onkeypress='return SomenteNumero(event)' onKeyPress="formata_mascara(this,'#.###','#')">
    </div>
    <div class="form-group col-md-2">
      <label>Estoque</label>
      <input type="text" class="form-control form-control-sm" id="estoque" name="estoque" value="<?php echo $estoque; ?>" readonly="true" maxlength="5" onkeypress='return SomenteNumero(event)'>
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-4">
  <label for="fornecedor">Fornecedor</label>
      <select id="fornecedor" class="form-control form-control-sm" name="fornecedor">
       <?php
         echo $htmlselect2;
       ?>
      </select>
</div> 
<div class="form-group col-md-5">
      <label>Foto</label>
      <input id="imagem" type="file" class="form-control form-control-sm" name="imagem" aria-label="Username" aria-describedby="sizing-addon1" maxlength="40">
     <!-- <input type="file" class="form-control form-control-sm" aria-describedby="sizing-addon1" id="imagem" name="imagem" maxlength="40"> -->
    </div>
</div>
  <div class="form-row">
  <div class="form-group col-md-10">
    <label>Receita</label>
    <textarea class="form-control form-control-sm" id="receita" name="receita" rows="3" maxlength="1000"><?php echo $receita; ?> </textarea>
  </div>
</div>
<button type="submit" class="btn btn-secondary">Gravar</button>
<a href="prod_grid.php?operacao=ativos"><button type="button" class="btn btn-secondary">Voltar</button></a>
  </form>
</body>
</html>