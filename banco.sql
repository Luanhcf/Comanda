-- Sequence: public.caixa_id_seq

-- DROP SEQUENCE public.caixa_id_seq;

CREATE SEQUENCE public.caixa_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.caixa_id_seq
  OWNER TO postgres;
-- Sequence: public.classificacao_id_seq

-- DROP SEQUENCE public.classificacao_id_seq;

CREATE SEQUENCE public.classificacao_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.classificacao_id_seq
  OWNER TO postgres;
-- Sequence: public.clientes_id_seq

-- DROP SEQUENCE public.clientes_id_seq;

CREATE SEQUENCE public.clientes_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.clientes_id_seq
  OWNER TO postgres;
-- Sequence: public.empresa_id_seq

-- DROP SEQUENCE public.empresa_id_seq;

CREATE SEQUENCE public.empresa_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.empresa_id_seq
  OWNER TO postgres;
-- Sequence: public.empresa_id_seq1

-- DROP SEQUENCE public.empresa_id_seq1;

CREATE SEQUENCE public.empresa_id_seq1
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.empresa_id_seq1
  OWNER TO postgres;
-- Sequence: public.formas_id_seq

-- DROP SEQUENCE public.formas_id_seq;

CREATE SEQUENCE public.formas_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.formas_id_seq
  OWNER TO postgres;
-- Sequence: public.fornecedores_id_seq

-- DROP SEQUENCE public.fornecedores_id_seq;

CREATE SEQUENCE public.fornecedores_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.fornecedores_id_seq
  OWNER TO postgres;
-- Sequence: public.funcionarios_id_seq

-- DROP SEQUENCE public.funcionarios_id_seq;

CREATE SEQUENCE public.funcionarios_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.funcionarios_id_seq
  OWNER TO postgres;
-- Sequence: public.mesas_id_seq

-- DROP SEQUENCE public.mesas_id_seq;

CREATE SEQUENCE public.mesas_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.mesas_id_seq
  OWNER TO postgres;
-- Sequence: public.mov_cxa_id_seq

-- DROP SEQUENCE public.mov_cxa_id_seq;

CREATE SEQUENCE public.mov_cxa_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.mov_cxa_id_seq
  OWNER TO postgres;
-- Sequence: public.mov_sai_id_seq

-- DROP SEQUENCE public.mov_sai_id_seq;

CREATE SEQUENCE public.mov_sai_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.mov_sai_id_seq
  OWNER TO postgres;
-- Sequence: public.params_id_seq

-- DROP SEQUENCE public.params_id_seq;

CREATE SEQUENCE public.params_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.params_id_seq
  OWNER TO postgres;
-- Sequence: public.pedidos_id_seq

-- DROP SEQUENCE public.pedidos_id_seq;

CREATE SEQUENCE public.pedidos_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.pedidos_id_seq
  OWNER TO postgres;
-- Sequence: public.pedidos_id_seq1

-- DROP SEQUENCE public.pedidos_id_seq1;

CREATE SEQUENCE public.pedidos_id_seq1
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.pedidos_id_seq1
  OWNER TO postgres;
-- Sequence: public.produtos_id_seq

-- DROP SEQUENCE public.produtos_id_seq;

CREATE SEQUENCE public.produtos_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.produtos_id_seq
  OWNER TO postgres;
-- Sequence: public.raster_id_seq

-- DROP SEQUENCE public.raster_id_seq;

CREATE SEQUENCE public.raster_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.raster_id_seq
  OWNER TO postgres;
-- Sequence: public.seq_atend

-- DROP SEQUENCE public.seq_atend;

CREATE SEQUENCE public.seq_atend
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.seq_atend
  OWNER TO postgres;
-- Sequence: public.seq_caixa

-- DROP SEQUENCE public.seq_caixa;

CREATE SEQUENCE public.seq_caixa
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.seq_caixa
  OWNER TO postgres;
-- Sequence: public.sitcaixa_id_seq

-- DROP SEQUENCE public.sitcaixa_id_seq;

CREATE SEQUENCE public.sitcaixa_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.sitcaixa_id_seq
  OWNER TO postgres;
-- Table: public.caixa

-- DROP TABLE public.caixa;

CREATE TABLE public.caixa
(
  id integer NOT NULL DEFAULT nextval('caixa_id_seq'::regclass),
  descricao character varying(60),
  CONSTRAINT caixa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.caixa
  OWNER TO postgres;
-- Table: public.classificacao

-- DROP TABLE public.classificacao;

CREATE TABLE public.classificacao
(
  id integer NOT NULL DEFAULT nextval('classificacao_id_seq'::regclass),
  descricao character varying(30),
  alerta boolean,
  imagem character varying(40),
  status boolean,
  CONSTRAINT classificacao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.classificacao
  OWNER TO postgres;

-- Table: public.clientes

-- DROP TABLE public.clientes;

CREATE TABLE public.clientes
(
  id integer NOT NULL DEFAULT nextval('clientes_id_seq'::regclass),
  pessoa character(1),
  status boolean,
  datacad date,
  nome character(50),
  apelido character(50),
  endereco character(50),
  bairro character(25),
  cidade character(25),
  cpf character(14),
  cnpj character(18),
  ie character(20),
  email character(50),
  telefone character(13),
  celular character(14),
  obs character(1000),
  CONSTRAINT clientes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.clientes
  OWNER TO postgres;
-- Table: public.empresa

-- DROP TABLE public.empresa;

CREATE TABLE public.empresa
(
  id integer NOT NULL DEFAULT nextval('empresa_id_seq1'::regclass),
  fantasia character varying(120),
  nome character varying(120),
  endereco character varying(120),
  bairro character varying(120),
  cidade character varying(120),
  cnpj character varying(18),
  insc character varying(14),
  email character varying(120),
  telefone character varying(13),
  celular character varying(14),
  CONSTRAINT empresa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.empresa
  OWNER TO postgres;
-- Table: public.formas

-- DROP TABLE public.formas;

CREATE TABLE public.formas
(
  id integer NOT NULL DEFAULT nextval('formas_id_seq'::regclass),
  descricao character varying(60),
  CONSTRAINT formas_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.formas
  OWNER TO postgres;
-- Table: public.fornecedores

-- DROP TABLE public.fornecedores;

CREATE TABLE public.fornecedores
(
  id integer NOT NULL DEFAULT nextval('fornecedores_id_seq'::regclass),
  status boolean,
  datacad date,
  fornecedor character varying(50),
  fantasia character varying(50),
  endereco character varying(50),
  bairro character varying(25),
  cidade character varying(25),
  cpf character varying(14),
  rg character varying(14),
  cnpj character varying(18),
  ie character varying(14),
  email character varying(50),
  celular character varying(14),
  telefone character varying(13),
  homepage character varying(50),
  forma character varying(20),
  obs character varying(1000),
  CONSTRAINT fornecedores_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fornecedores
  OWNER TO postgres;
-- Table: public.funcionarios

-- DROP TABLE public.funcionarios;

CREATE TABLE public.funcionarios
(
  id integer NOT NULL DEFAULT nextval('funcionarios_id_seq'::regclass),
  status boolean,
  datacad date,
  nome character varying(50),
  apelido character varying(50),
  endereco character varying(50),
  bairro character varying(25),
  cidade character varying(25),
  cpf character varying(14),
  rg character varying(14),
  celular character varying(14),
  email character varying(50),
  comissao numeric(3,2),
  telefone character varying(13),
  obs character varying(1000),
  funcao character varying(7),
  CONSTRAINT funcionarios_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.funcionarios
  OWNER TO postgres;
-- Table: public.mesas

-- DROP TABLE public.mesas;

CREATE TABLE public.mesas
(
  id integer NOT NULL DEFAULT nextval('mesas_id_seq'::regclass),
  descricao character varying(50),
  ocupada boolean DEFAULT false,
  atend numeric(13,0),
  CONSTRAINT mesas_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mesas
  OWNER TO postgres;
-- Table: public.mov_cxa

-- DROP TABLE public.mov_cxa;

CREATE TABLE public.mov_cxa
(
  id integer NOT NULL DEFAULT nextval('mov_cxa_id_seq'::regclass),
  caixa numeric(2,0),
  data date,
  forma numeric(2,0),
  valor numeric(13,2),
  historico character varying(60),
  tipo character varying(1),
  chave_atend numeric(13,0),
  CONSTRAINT mov_cxa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mov_cxa
  OWNER TO postgres;
-- Table: public.mov_sai

-- Table: mov_sai

-- DROP TABLE mov_sai;

CREATE TABLE mov_sai
(
  id integer NOT NULL DEFAULT nextval('mov_sai_id_seq'::regclass),
  id_mesa numeric(13,0),
  atend numeric(13,0),
  produtos numeric(13,0),
  quant numeric(13,2),
  func numeric(13,0),
  total numeric(13,2),
  cliente numeric(2,0),
  data timestamp without time zone,
  detalhe character varying(30),
  chavecx numeric(13,0),
  CONSTRAINT mov_sai_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mov_sai
  OWNER TO postgres;

-- Table: public.params

-- DROP TABLE public.params;

CREATE TABLE public.params
(
  id integer NOT NULL DEFAULT nextval('params_id_seq'::regclass),
  cript character varying(40),
  cli_pad numeric(2,0),
  taxas numeric(13,2),
  individual boolean NOT NULL DEFAULT true,
  imagem character varying(40),
  CONSTRAINT params_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.params
  OWNER TO icomp;

-- Table: public.pedidos

-- DROP TABLE public.pedidos;

-- Table: pedidos

-- DROP TABLE pedidos;

CREATE TABLE pedidos
(
  id integer NOT NULL DEFAULT nextval('pedidos_id_seq1'::regclass),
  codprod numeric(10,0),
  id_mesa numeric(10,0),
  atend numeric(10,0),
  func numeric(10,0),
  quant numeric(10,2),
  data timestamp with time zone,
  dataf timestamp with time zone,
  finaliza character varying(1) DEFAULT 'f'::character varying,
  CONSTRAINT pedidos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pedidos
  OWNER TO postgres;

-- Table: public.produtos

-- DROP TABLE public.produtos;

CREATE TABLE public.produtos
(
  id integer NOT NULL DEFAULT nextval('produtos_id_seq'::regclass),
  status boolean,
  datacad date,
  class numeric(2,0),
  descricao character varying(120),
  codbar numeric(15,0),
  und character varying(3),
  estoque numeric(5,0),
  receita character varying(120),
  complemento character varying(50),
  imagem character varying(40),
  pr_compra numeric(13,3),
  pr_custo numeric(13,3),
  pr_venda numeric(13,3),
  fornecedor numeric(2,0),
  marca character varying(15),
  embalagem character varying(15),
  tipo character varying(1), -- P - CONTROLA ESTOQUE // N - NÃO CONTROLA ESTOQUE
  CONSTRAINT produtos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.produtos
  OWNER TO postgres;
COMMENT ON COLUMN public.produtos.tipo IS 'P - CONTROLA ESTOQUE // N - NÃO CONTROLA ESTOQUE';
-- Table: public.raster

-- DROP TABLE public.raster;

CREATE TABLE public.raster
(
  id integer NOT NULL DEFAULT nextval('raster_id_seq'::regclass),
  codigo integer,
  tipo character varying(1),
  motivo character varying(120),
  data timestamp without time zone,
  quant integer,
  ajust boolean,
  atend numeric(13,0),
  CONSTRAINT raster_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.raster
  OWNER TO postgres;
-- Table: public.sitcaixa

-- DROP TABLE public.sitcaixa;

CREATE TABLE public.sitcaixa
(
  id integer NOT NULL DEFAULT nextval('sitcaixa_id_seq'::regclass),
  codigo numeric(13,0),
  data date,
  fechado boolean,
  CONSTRAINT sitcaixa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.sitcaixa
  OWNER TO postgres;
-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE public.usuarios
(
  usuario character varying(20) NOT NULL,
  senha character varying(32),
  podeinserir integer,
  nivel character varying(13),
  status boolean,
  func integer,
  bloquea_op boolean DEFAULT false,
  CONSTRAINT usuarios_pkey PRIMARY KEY (usuario)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuarios
  OWNER TO postgres;



insert into funcionarios (id,nome,status) values (99,'ADMINISTRADOR','t');
insert into usuarios (usuario,senha,podeinserir,nivel,status,func) values ('adm','b09c600fddc573f117449b3723f23d64','1','Administrador','t',99);
insert into params (cript,cli_pad) values ('asdas','1');
update params set cript=encode(encode((current_date+30)::text::bytea, 'base64')::text::bytea,'base64'); -- validação inicial
--select encode(encode('2018-01-01','base64')::text::bytea,'base64'); -- criar arquivo de validação
update params set taxas=10;