<?php
session_start();
require_once 'config/conexao.php';
$erro = isset($_GET['erro']) ? $_GET['erro'] : '';
$val = isset($_GET['val']) ? $_GET['val'] : '';
$msg="";
$msg="<div class=\"alert alert-danger\">
<strong>Login ou senha incorretos!</strong> Verifique seus dados.
</div>";
$msg2="";
$msg2="<div class=\"alert alert-danger\">
<strong>Sistema expirado!</strong> Entre em contato com o suporte.
</div>";
$msg3="";
$msg3="<div class=\"alert alert-danger\">
<strong>Usuario inativo!<br></strong> Entre em contato com o Administrador do sistema.
</div>";
$msg4="";
$msg4="<div class=\"alert alert-danger\">
<strong>Usuario e senha vazios!<br></strong> Preencha os dados corretamente.
</div>";
$msg5="";
$msg5="<div class=\"alert alert-danger\">
<strong>Senha não preenchida!<br></strong> Preencha os dados corretamente.
</div>";
$msg6="";
$msg6="<div class=\"alert alert-danger\">
<strong>Usuario não cadastrado!<br></strong> Preencha os dados corretamente.
</div>";
$msg7="";
$msg7="<div class=\"alert alert-success\" role=\"alert\">
Sistema validado com sucesso! </div>";

$sql1 = "select * From params";
$res1 = pg_query($conexao,$sql1);
$row = pg_fetch_assoc($res1);
$validade = base64_decode(base64_decode($row["cript"]));
?>
<!DOCTYPE html>
<html lang="en">
   <!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->
   <head>
      <title>Login</title>
      <meta charset="utf-8">
      <link href="boot/menu.css" rel="stylesheet">
      <link href="boot/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <script type="text/javascript" src="boot/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="boot/js/bootstrap.min.js"></script>
   </head>
   <body class="bg-dark">
      <br><br>
      <div class="container py-5">
         <div class="row">
            <div class="col-md-12">
            <h1 align="center" ><span class="badge badge-secondary">Osíris 1.0</span></h1> 
             <!--<img src="img/logo.jpeg" class="img-circle" alt="Cinque Terre" align="top" width="180" height="150"> -->
               <div class="row">
                  <div class="col-md-6 mx-auto">
                     <!-- form card login -->
                     <div class="card rounded-0">
                        <div class="card-header">
                           <h3 class="mb-0">Login</h3>
                        </div>
                        <div class="card-body">
                        <form name="login" method="POST" action="validalogin.php">
                              <div class="form-group">
                                 <label for="uname1">Usuario</label>
                                 <input type="text" class="form-control form-control-lg rounded-0" name="login" id="login">
                                 <div class="invalid-feedback">Oops, you missed this one.</div>
                              </div>
                              <div class="form-group">
                                 <label>Senha</label>
                                 <input type="password" class="form-control form-control-lg rounded-0" id="senha"  name="senha" autocomplete="new-password">
                                 <div class="invalid-feedback">Enter your password too!</div>
                              </div>
                              <?php
                               if ($erro == 1){
                                 print($msg);
                                }else if ($erro==2){
                                  print($msg2);
                                }else if ($erro==3){
                                  print($msg3);
                                }else if ($erro==4){
                                  print($msg4);
                                }else if ($erro==5){
                                  print($msg5);
                                }else if ($erro==6){
                                  print($msg6);
                                }
                                 if($val==1){
                                   print($msg7);
                                 }
                               ?>
                              <a href="vs.php"> <label align="left">Validação</label></a>&nbsp&nbsp&nbsp&nbsp&nbsp
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                              <label>Licença valida até: <?php echo $validade = date("d-m-Y", strtotime($validade)); ?> </label>
                            <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button
                            >
                           </form>
                        </div>
                        <!--/card-block-->
                     </div>
                  </div>
               </div>
               <!--/row-->
            </div>
            <!--/col-->
         </div>
         <!--/row-->
      </div>
      <!--/container-->
   </body>
</html>

