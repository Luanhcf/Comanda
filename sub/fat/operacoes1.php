<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$operacoes = isset($_GET['operacoes']) ? $_GET['operacoes'] : '';
$idmesa = isset($_GET['idmesa']) ? $_GET['idmesa'] : '';
$titulo="";

$sql="select 
pe.id as id_ped,
m.id as id_mov,
p.descricao,
m.atend
FROM   mov_sai m
INNER JOIN mesas me
        ON ( m.atend = me.atend )
INNER JOIN produtos p
        ON ( m.produtos = p.id )
INNER JOIN classificacao cl
        ON (p.class=cl.id)  
INNER JOIN pedidos pe
         ON  (pe.atend = m.atend and pe.codprod=p.id)             
WHERE  ocupada = 't'
AND me.id = $idmesa
AND total > 0
AND cl.alerta='t'";

$res = pg_query($conexao,$sql);

$select ="";

while ($row=pg_fetch_assoc($res)){

  $select=$select.("<option value=\"".trim($row["id_mov"]).":".trim($row["id_ped"])."\">".$row["descricao"]."</option>");
}

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $titulo; ?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_cadcli.js"
  ></script>

</head>
<html>
<body>
<form  name="cad_classi" method="post" action="opajax.php" enctype="multipart/form-data">
<h2 align="center">Fracionar produto</h2>
<br>
    <input  name="operacoes" type="hidden" value='<?php echo $operacoes; ?>'/>
    <input  name="idmesa" type="hidden" value='<?php echo $idmesa; ?>'/>
    <div align="center">
    <label>Selecionar produto </label>
    <br>
    <select name="produto" id="produto" class="form-control form-control-sm col-md-3" >
      <?php
      print("$select");
      ?>
    </select>
      <label>Divisor</label>
      <select name="divisor" id="divisor" class="form-control form-control-sm col-md-1">
        <option value="2">1/2</option>
        <option value="3">1/3</option>
        <option value="4">1/4</option>
      </select>
      <br>
      <button type="submit" class="btn btn-secondary">Aplicar</button>
</div>
</form>
</body>
</html>