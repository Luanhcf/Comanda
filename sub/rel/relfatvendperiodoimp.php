<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$datai = isset($_POST['datai']) ? $_POST['datai'] : date('Y-m-d');
$dataf = isset($_POST['dataf']) ? $_POST['dataf'] : date('Y-m-d');
//SQL COM OS DADOS DA EMPRESA
$sql1="select * From empresa";
$res1= pg_query($conexao,$sql1);
$row1= pg_fetch_assoc($res1);
//SQL DO RELATORIO
$sql="select id_mesa,
mov_sai.atend,
clientes.nome as cliente,
-- funcionarios.nome as funcionario,
sum(coalesce(total,0)) as total,
case 
when mov_sai.atend not in (select atend from mesas where atend > 0)  then 'Fechada'
when mesas.atend = mov_sai.atend then 'Aberta' else '?' end as situacao,
to_char(data,'dd-mm-yyyy') AS data
FROM     mov_sai inner join funcionarios on (mov_sai.func=funcionarios.id) inner join clientes on (clientes.id=mov_sai.cliente)
inner join mesas on (mov_sai.id_mesa=mesas.id)
WHERE   date(mov_sai.data) between '$datai' AND '$dataf'
GROUP BY id_mesa,
mov_sai.atend,
--funcionarios.nome,
clientes.nome,
mesas.atend,
to_char(data,'dd-mm-yyyy')	
ORDER BY atend desc
";
$res = pg_query($conexao,$sql);
$sql3="select 
sum(coalesce(total,0)) as total
FROM     mov_sai inner join funcionarios on (mov_sai.func=funcionarios.id) inner join clientes on (clientes.id=mov_sai.cliente)
inner join mesas on (mov_sai.id_mesa=mesas.id)
WHERE    date(mov_sai.data) between '$datai' AND '$dataf'
";
$res3=pg_query($conexao,$sql3);
$row3=pg_fetch_assoc($res3);
?>

<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Vendas no periodo</title>

  <meta charset="utf-8"></meta>


  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  .table thead th {

vertical-align: bottom;
border-bottom: 1px solid #000;
border-top: 1px solid #000;
}
.th {
text-align: center;
}
.td {
text-align: center;
}
  </style>

</head>
<body>
<div align="left">
<br>
<h3><b>&nbsp&nbsp<?php echo $row1['fantasia']; ?></b> </h3>
&nbsp&nbsp&nbsp&nbsp<?php echo strtoupper($namesis); ?> - SISTEMA DE GERENCIAMENTO DE MESAS
<br>
<label>&nbsp&nbsp&nbsp RELATORIO DE VENDAS NO PERIODO DE <?php echo $datai; ?> A <?php echo $dataf; ?></label>
</div>  

<table align="center" class="table table-sm" width="50%" >
    <thead>
      <tr>
        <th>Mesa</th>
        <th>Nº</th>
        <th>Cliente</th>
        <th>Situação</th>
        <th>Data</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody id="myTable">
    <?php
    $select ="";
   while ($row2=pg_fetch_assoc($res)){
    $select="<tr>
    <td>".$row2['id_mesa']."</td>
    <td>".$row2['atend']."</td>
    <td>".$row2['cliente']."</td>
    <td><font color=\"".(trim($row2["situacao"]) == "Fechada" ? "red" : "green")."\">".$row2['situacao']."</font></td>
    <td>".$row2['data']."</td>
    <td>".$row2['total']."</td>
    </tr>";
    print("$select");
  }
  ?>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL: <?php echo $row3['total']; ?></b></td>
  </table>
  
</body>


