<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis  = $_SESSION["namesis"];
$datai    = isset($_POST['datai']) ? $_POST['datai'] : date('Y-m-d');
$dataf    = isset($_POST['dataf']) ? $_POST['dataf'] : date('Y-m-d');
$caixa    = isset($_POST['caixa']) ? $_POST['caixa'] : '';
//SQL COM OS DADOS DA EMPRESA
$sql1="select * From empresa";
$res1= pg_query($conexao,$sql1);
$row1= pg_fetch_assoc($res1);
//SQL DO RELATORIO
$sqlgrident="select s.id,
case when s.chave_atend >= 0 then s.chave_atend else '00' end as chave_atend,
s.historico,
f.descricao,
s.tipo,
s.data,
s.valor
FROM   mov_cxa s
INNER JOIN formas f
        ON ( s.forma = f.id )
WHERE  caixa = $caixa and tipo='E'
AND data between '$datai' and '$dataf'
ORDER  BY id ASC";
$resgrid=pg_query($conexao,$sqlgrident);
$exibicao = "";
$exibicao2 = "";
$exibicao3 = "";
$exibicao4 = "";
$sqltotent="select sum(valor) as valor from mov_cxa where caixa = $caixa and tipo='E' AND data between '$datai' and '$dataf'";
$sqlresent = pg_query($conexao,$sqltotent);
$rowent = pg_fetch_assoc($sqlresent);
$sqlgridsai="select s.id,
case when s.chave_atend >= 0 then s.chave_atend else '00' end as chave_atend,
s.historico,
f.descricao,
s.tipo,
s.data,
s.valor
FROM   mov_cxa s
INNER JOIN formas f
        ON ( s.forma = f.id )
WHERE  caixa = $caixa and tipo='S'
AND data between '$datai' and '$dataf'
ORDER  BY id ASC";
$resgrid1=pg_query($conexao,$sqlgridsai);
$exibicao1 = "";
$sqltotsaid="select sum(valor) as valor from mov_cxa where caixa = $caixa and tipo='S' AND data between '$datai' and '$dataf'";
$sqlressaid = pg_query($conexao,$sqltotsaid);
$rowsai = pg_fetch_assoc($sqlressaid);

$listaform ="select descricao,sum(valor1)as valor from (
  select s.id,
  case when s.chave_atend >= 0 then s.chave_atend else '00' end as chave_atend,
  s.historico,
  f.descricao,
  s.tipo,
  s.data,
  s.valor,
  s.valor * case when s.tipo='S' then -1 else '1' end as valor1
  FROM   mov_cxa s
  INNER JOIN formas f
          ON ( s.forma = f.id )
  WHERE  caixa = $caixa 
  AND data between '$datai' and '$dataf'
  ORDER  BY id asc) as valor2
  group by descricao
  ";
$reslistform= pg_query($conexao,$listaform);

$listaform1 ="select descricao,sum(valor)as valor from (
  select s.id,
  case when s.chave_atend >= 0 then s.chave_atend else '00' end as chave_atend,
  s.historico,
  f.descricao,
  s.tipo,
  s.data,
  s.valor
  FROM   mov_cxa s
  INNER JOIN formas f
          ON ( s.forma = f.id )
  WHERE  caixa = $caixa
  AND data between '$datai' and '$datai' and s.tipo='S'
  ORDER  BY id asc) as valor2
  group by descricao
  ";
$reslistform1= pg_query($conexao,$listaform1);

$listaform2 ="select descricao,sum(valor)as valor from (
  select s.id,
  case when s.chave_atend >= 0 then s.chave_atend else '00' end as chave_atend,
  s.historico,
  f.descricao,
  s.tipo,
  s.data,
  s.valor
  FROM   mov_cxa s
  INNER JOIN formas f
          ON ( s.forma = f.id )
  WHERE  caixa = $caixa
  AND data between '$datai' and '$datai' and s.tipo='E'
  ORDER  BY id asc) as valor2
  group by descricao
  ";
$reslistform2= pg_query($conexao,$listaform2);

$totales="select 
sum(case when tipo='E' then valor*1 else valor*-1 end) as valor
from mov_cxa where data between '$datai' and '$dataf' and caixa=$caixa";
$restot = pg_query($conexao,$totales);
$rowtot = pg_fetch_assoc($restot);
?>

<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Movimentação de Caixa</title>

  <meta charset="utf-8"></meta>


  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  .table td, .table th{
    border-top: 0px;
  }
  body {
    text-align:center;
  }
  .table thead th {

vertical-align: bottom;
border-bottom: 1px solid #000;
border-top: 1px solid #000;
}
.th {
text-align: center;
}
.td {
text-align: center;
}
  </style>

</head>
<body>
<div align="left">
<br>
<h3><b>&nbsp&nbsp<?php echo $row1['fantasia']; ?></b> </h3>
&nbsp&nbsp&nbsp&nbsp<?php echo strtoupper($namesis); ?> - SISTEMA DE GERENCIAMENTO DE MESAS
<br>
<label>&nbsp&nbsp&nbsp RELATORIO DE MOVIMENTAÇÃO DE CAIXA DE:&nbsp<?php echo $datai; ?> A <?php echo $dataf; ?></label>
</div>  
<h5 aling="left"><b>ENTRADAS</b></h5>
<!--    GRID DE ENTRADAS     -->
<table align="center" class="table table-sm" width="50%" >
    <thead>
      <tr>
        <th>VENDA</th>
        <th>HISTORICO</th>
        <th>FORMA DE PGTO</th>
        <th>DATA</th>
        <th>TOTAL</th>
      </tr>
    </thead>
    <tbody id="myTable">
    <?php
    $select ="";
   while ($row2=pg_fetch_assoc($resgrid)){
    $exibicao="<tr>
    <td>".$row2['chave_atend']."</td>
    <td>".$row2['historico']."</td>
    <td>".$row2['descricao']."</td>
    <td>".$row2['data']."</td>
    <td>".$row2['valor']."</td>
    </tr>";
    print("$exibicao");
  }
  ?>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL: <?php echo $rowent['valor']; ?></b></td>
  </table>
  <h5 aling="left"><b>SAIDAS</b></h5>
  <!--    GRID DE SAIDAS     -->
  <table align="center" class="table table-sm" width="50%" >
    <thead>
      <tr>
        <th>VENDA</th>
        <th>HISTORICO</th>
        <th>FORMA DE PGTO</th>
        <th>DATA</th>
        <th>TOTAL</th>
      </tr>
    </thead>
    <tbody id="myTable1">
    <?php
    $select ="";
   while ($row4=pg_fetch_assoc($resgrid1)){
    $exibicao1="<tr>
    <td>".$row4['chave_atend']."</td>
    <td>".$row4['historico']."</td>
    <td>".$row4['descricao']."</td>
    <td>".$row4['data']."</td>
    <td>".$row4['valor']."</td>
    </tr>";
    print("$exibicao1");
  }
  ?>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL: <?php echo $rowsai['valor']; ?></b></td>
  </table>

  <h5><b>SALDOS DE ENTRADA</b></h5>
  <table align="left" class="table table-sm" width="50%" >
  <?php
  while ($row5=pg_fetch_assoc($reslistform2)){
    $exibicao3="<tr>
    <td >".$row5['descricao']." R$:".$row5['valor']."</td>
    </tr>";
    print("$exibicao3");
  }
    ?>
    </table>

    <h5><b>SALDOS DE SAIDA</b></h5>
  <table align="left" class="table table-sm" width="50%" >
  <?php
  while ($row6=pg_fetch_assoc($reslistform1)){
    $exibicao4="<tr>
    <td>".$row6['descricao']." R$:".$row6['valor']."</td>
    </tr>";
    print("$exibicao4");
  }
    ?>
    </table>

    <h5><b>SALDOS DE (ENTRADA - SAIDAS)</b></h5>
  <table align="left" class="table table-sm" width="50%" >
  <?php
  while ($row7=pg_fetch_assoc($reslistform)){
    $exibicao2="<tr>
    <td>".$row7['descricao']." R$:".$row7['valor']."</td>
    </tr>";
    print("$exibicao2");
  }
    ?>
  <tr>
  <td><b><u>TOTAL R$: <?php echo $rowtot['valor']; ?></u></b> </td>
  </tr>
    </table>
</body>


