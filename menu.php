<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once 'config/conexao.php';
$userlogado = $_SESSION["usuario"];
$sqlvernivel = "select nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$namesis = $_SESSION["namesis"];
$sql ="select * From params";
$res = pg_query($conexao,$sql);
$row = pg_fetch_assoc($res);
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->
<head>
  <title>Menu Principal</title>
  <meta charset="utf-8">
  <link href="boot/menu.css" rel="stylesheet">
  <link href="iconss/css/all.css" rel="stylesheet">
  <link href="boot/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script type="text/javascript" src="boot/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="boot/fumenu.js"></script>
  <script type="text/javascript" src="boot/js/bootstrap.min.js"></script>
  <style>
  .table thead th{
    border-bottom: 0px;
  }
  .table td, .table th{
    border-top: 0px;
  }

  body {
    text-align:center;
  }

  </style>
</head>
<body>
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <span></span> <a class="navbar-brand" href="menu.php"><?php echo $namesis; ?></a></span>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
        <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/fat/faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"sub/conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/fat/faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/fat/faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/cxa/caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>
        
  <br>
    <!-- Fim do desenho do menu -->
  <!-- Desenho do Menu -->
  <img src="img/<?php echo $row['imagem']; ?>" width="300" class="img-fluid" alt="Responsive image"> 
 <?php
      //Administrador -> 1
 /*    if ($_SESSION ["nivel"] == 1 ){
     $sqlgrid="select * from mesas order by descricao";
     $res=pg_query($conexao,$sqlgrid);
     $htmlselect3="";
     $cont = 0;

 print("<h2>Monitoramento de Mesas e Cozinha</h2>
 <br>
 <div class=\"table-responsive\">
    <table class=\"table\">
      <thead>");

      while ($row=pg_fetch_assoc($res)){
        $cont = $cont+1;
        $htmlselect3.= "<th><img src=\"img/mesa.png\" width=\"55px\"><br><font color=\"".(trim($row["ocupada"]) == "t" ? "red" : "green").
        "\" size=\"4\">".$row["descricao"]."<br>".(trim($row["ocupada"]) == "t" ? "Ocupada" : "Livre")."</th>";
        if($cont == 10){
          print("<tr> $htmlselect3 </tr>");
          $htmlselect3 ='';
          $cont= 0;
        }}
        if($cont > 0){
         print("<tr> $htmlselect3 </tr>");
         $htmlselect3 ='';
         $cont= 0;
       }

     print ("</thead>
     </table>
     </div>");

      //Garcom -> 2
     }else if ($_SESSION ["nivel"] == 2 ){
      $sqlgrid="select * from mesas order by descricao";
     $res=pg_query($conexao,$sqlgrid);
     $htmlselect3="";
     $cont = 0;

 print("<h2>Monitoramento de Mesas e Cozinha</h2>
 <br>
 <div class=\"table-responsive\">
    <table class=\"table\">
      <thead>");

      while ($row=pg_fetch_assoc($res)){
        $cont = $cont+1;
        $htmlselect3.= "<th><img src=\"img/mesa.png\" width=\"55px\"><br><font color=\"".(trim($row["ocupada"]) == "t" ? "red" : "green").
        "\" size=\"4\">".$row["descricao"]."<br>".(trim($row["ocupada"]) == "t" ? "Ocupada" : "Livre")."</th>";
        if($cont == 10){
          print("<tr> $htmlselect3 </tr>");
          $htmlselect3 ='';
          $cont= 0;
        }}
        if($cont > 0){
         print("<tr> $htmlselect3 </tr>");
         $htmlselect3 ='';
         $cont= 0;
       }

     print ("</thead>
     </table>
     </div>");
     // Cozinha -> 3
     }else if ($_SESSION ["nivel"] == 3 ){
     
      print("<span>
      <h2>Pedidos da Cozinha</h2>
      <br>
      <table class=\"table table-sm\">
           <thead>
           <tr>
             <th>Ação</th>
             <th>Mesa</th>
             <th>Pedido</th>
             <th>Garcom</th>
             <th>Hora</th>
             <th>Obs</th>
          </tr>
          <tr>
          <td><i class=\"far fa-thumbs-up\"></i>&nbsp&nbsp&nbsp<i class=\"far fa-thumbs-down\"></i></td>
          <td>01</td>
          <td>Filé com Fritas</td>
          <td>Lula presidiario</td>
          <td>08:30</td>
          <td>Com muita Batata</td>
          </tr>
          <tr>
          <td><i class=\"far fa-thumbs-up\"></i>&nbsp&nbsp&nbsp<i class=\"far fa-thumbs-down\"></i></td>
          <td>03</td>
          <td>500 gramas picanha refeição</td>
          <td>Lula ladrão</td>
          <td>09:00</td>
          <td>Picanha</td>
          </tr>
          </thead>
          </table>");
     }
     // Caixa -> 4
     else if ($_SESSION ["nivel"] == 4 ){
      print(" <h2>Movimentação de caixa</h2>
      <br>");
     } */
     ?> 
     

</body>

</html>