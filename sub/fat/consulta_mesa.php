<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
require_once '../../config/conexao.php';
$userlogado = $_SESSION["usuario"];
$sqlvernivel = "select nivel from usuarios where usuario='$userlogado'";
$resvernivel = pg_query($conexao,$sqlvernivel);
$rowvernivel = pg_fetch_assoc($resvernivel);
$namesis = $_SESSION["namesis"];
$datai = isset($_POST['datai']) ? $_POST['datai'] : date('Y-m-d');
$dataf = isset($_POST['dataf']) ? $_POST['dataf'] : date('Y-m-d');

$sql="select id_mesa,
mov_sai.atend,
clientes.nome as cliente,
round(sum(total),2) as total,
case 
when mov_sai.atend not in (select atend from mesas where atend > 0)  then 'Fechada'
when mesas.atend = mov_sai.atend then 'Aberta' else '?' end as situacao,
to_char(data,'dd-mm-yyyy') AS data
FROM     mov_sai 
inner join clientes on (clientes.id=mov_sai.cliente)
inner join mesas on (mov_sai.id_mesa=mesas.id)
WHERE    --quant > 0 AND
      date(mov_sai.data) between '$datai' AND '$dataf'
GROUP BY id_mesa,
mov_sai.atend,
clientes.nome,
mesas.atend,
to_char(data,'dd-mm-yyyy')	
ORDER BY atend desc
";
$select="";
$res=pg_query($conexao,$sql);
$sql1="select sum(coalesce(total,0)) as total from mov_sai where date(data) between '$datai' and '$dataf'";
$res1=pg_query($conexao,$sql1);
$row1=pg_fetch_assoc($res1);
?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title>Consulta Mesas</title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">
  
  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>
  <link
    href="../../boot/jqueryui/jquery-ui.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../boot/jqueryui/jquery-ui.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>

  <style>
  body {
    text-align:center;
  }
  </style>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
 <script>
      $( function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker" ).val("<?php echo $datai; ?>");
      } );
    </script>
    <script>
      $( function() {
        $( "#datepicker2" ).datepicker();
        $( "#datepicker2" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        $( "#datepicker2" ).val("<?php echo $dataf; ?>");
      } );
    </script>
</head>

<body>
<form method="POST">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
        if($rowvernivel['nivel'] == 'Administrador'){
        print("<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
        <ul class=\"navbar-nav mr-auto\">
          <li class=\"nav-link\">
            <a class=\"nav-link\" href=\"../../menu.php\">
              <i class=\"fa fa-home\"></i>
              Inicio
              <!--   <span class=\"sr-only\">(current)</span> -->
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cad/cadastro.php\">
          <i class=\"fa fa-clipboard\"></i>
          Cadastros
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../est/estoque.php\">
          <i class=\"fa fa-box\"></i>
          Estoque
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"faturamento.php\">
          <i class=\"fa fa-shopping-cart\"></i>
          Operação
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../cxa/caixa.php\">
          <i class=\"fa fa-money-bill-alt\"></i>
          Caixa
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../rel/relatorios.php\">
          <i class=\"fa fa-chart-line\"></i>
          Relatorios
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../conf/configuracoes.php\">
          <i class=\"fa fa-cogs\"></i>
          Configurações
        </a>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"../../logout.php\">
            <i class=\"fa fa-times-circle\"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class=\"nav-item\">
         <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
        </li>
      </ul>
    </div>
  </nav>");
        }
        if($rowvernivel['nivel'] == 'Garcom'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Cozinha'){ 
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"..\..\menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"faturamento.php\">
            <i class=\"fa fa-shopping-cart\"></i>
            Operação
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"..\..\logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
        }
        if($rowvernivel['nivel'] == 'Caixa'){
          print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
          <ul class=\"navbar-nav mr-auto\">
            <li class=\"nav-link\">
              <a class=\"nav-link\" href=\"menu.php\">
                <i class=\"fa fa-home\"></i>
                Inicio
                <!--   <span class=\"sr-only\">(current)</span> -->
              </a>
            </li>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/cxa/caixa.php\">
            <i class=\"fa fa-money-bill-alt\"></i>
            Caixa
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
          <a class=\"nav-link\" href=\"sub/rel/relatorios.php\">
            <i class=\"fa fa-chart-line\"></i>
            Relatorios
          </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"logout.php\">
              <i class=\"fa fa-times-circle\"></i>
              Sair
            </a>
          </li>
          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
          <li class=\"nav-item\">
           <b><font color=\"white\">Usuário:&nbsp&nbsp ".strtoupper("$userlogado")."  </font></b>
          </li>
        </ul>
      </div>
    </nav>");
          }
        ?>

  <!-- --------------------------------------Fim do desenho do menu----------------------------------------------- -->
  <!-- Desenho do cadastro -->
  <div class="container">
  <br>
  <h2>Movimentação de mesas</h2> 
  <button type="submit" class="btn btn-secondary">Filtrar </button> 
  <a href="faturamento.php"><button type="button" class="btn btn-secondary"> Voltar </button><a/>
  <div align="left">
  <label><b>Data inicial</b></label>
  <input class="form-control form-control-sm col-md-2" name ="datai" id="datepicker" size="30" type="text" value="<?php echo $datai; ?>">
  <label><b>Data Final</b></label>
  <input class="form-control form-control-sm col-md-2" name ="dataf" id="datepicker2" type="text"  value="<?php echo $dataf; ?>">
  <br>  
  <input class="form-control form-control-sm col-md-4" id="myInput" type="text" placeholder="Buscar ..">
  <label><b>Total: R$: <?php echo $row1['total']; ?></b></label>   
</div>
  <p align="center">
</p>
  <table class="table table-sm">
    <thead>
      <tr>
        <th>Mesa</th>
        <th>Nº Atendimento</th>
        <th>Cliente</th>
        <!--<th>Funcionário</th> -->
        <th>Total</th>
        <th>Situação</th>
        <th>Data</th>
        <th>Imprimir</th>
      </tr>
    </thead>
    <tbody id="myTable">
    <?php
   while ($row=pg_fetch_assoc($res)){
    $select="<tr>
    <td>".$row['id_mesa']."</td>
    <td>".$row['atend']."</td>
    <td>".$row['cliente']."</td>
    <td>R$:".$row['total']."</td>
    <td><b><font color=\"".(trim($row["situacao"]) == "Fechada" ? "red" : "green")."\">".$row['situacao']."</font></b></td>
    <td>".$row['data']."</td>
    <td><a href=\"imprimeconta1.php?atend=".$row['atend']."\" target=\"_blank\"><i class=\"fas fa-print\"></i></a></td>
    </tr>";
    print("$select");
  }
  ?>
  </table>
  </form>
</body>
</html>