<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("../../config/conexao.php");
$userlogado = $_SESSION["usuario"];
$namesis = $_SESSION["namesis"];
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="<div class=\"alert alert-success\" role=\"alert\">Registros atualizados com sucesso!</div>";
$sql="select * From params";
$res=pg_query($conexao,$sql);
$row=pg_fetch_assoc($res);
//Variaveis retornando do banco de dados
$cli_pad     = trim($row['cli_pad']);
$taxas = trim($row['taxas']);
$individual = trim($row['individual']);
$nome1    = 'Parametros do sistema';

//CONSULTANDO CLIENTES BANCO DE DADOS
$sqlcli = "select * from clientes where status='t'";
$rescli = pg_query($conexao,$sqlcli); 
$htmlselect= "";
while ($row=pg_fetch_assoc($rescli)){

  $htmlselect=$htmlselect.("<option value=\"".trim($row["id"])."\" ".(trim($row["id"]) == trim($cli_pad) ? "selected" : "").">".$row["nome"]."</option>");
}

?>
<!DOCTYPE html>
<html lang="en">
<!-- SISTEMA DESENVOLVIDO POR LUAN HENRIQUE COSTA FONSECA -->

<head>
  <title><?php echo $nome;?></title>

  <meta charset="utf-8"></meta>

  <link href="../../iconss/css/all.css" rel="stylesheet">

  <link
    href="../../boot/menu.css"
    rel="stylesheet"
  ></link>

  <link
    href="../../boot/css/bootstrap.min.css"
    rel="stylesheet"
    id="bootstrap-css"
  ></link>

  <script
    type="text/javascript"
    src="../../boot/jquery-3.3.1.min.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/fumenu.js"
  ></script>

  <script
    type="text/javascript"
    src="../../boot/js/bootstrap.min.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_empres.js"
  ></script>
  <script
    type="text/javascript"
    src="../../func/func_empres.js"
  ></script>
  <script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>45 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>
</head>

<body>
<form  name="cad_empres" method="post" action="../../rec/empresajax.php" enctype="multipart/form-data">
  <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
    <a
      class="navbar-brand"
      href="../../menu.php"
    ><?php echo $namesis; ?></a>

    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div
      class="collapse navbar-collapse"
      id="navbarSupportedContent"
    >
      <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          <a
            class="nav-link"
            href="../../menu.php"
          >
            <i class="fa fa-home"></i>
            Inicio

            <!-- <span class="sr-only">(current)</span> -->
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cad/cadastro.php"
          >
            <i class="fa fa-clipboard"></i>
            Cadastros
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../est/estoque.php"
          >
            <i class="fa fa-box"></i>
            Estoque
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../fat/faturamento.php"
          >
            <i class="fa fa-shopping-cart"></i>
            Operação
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../cxa/caixa.php"
          >
            <i class="fa fa-money-bill-alt"></i>
            Caixa
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../rel/relatorios.php"
          >
            <i class="fa fa-chart-line"></i>
            Relatorios
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../conf/configuracoes.php"
          >
            <i class="fa fa-cogs"></i>
            Configurações
          </a>
        </li>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

        <li class="nav-item">
          <a
            class="nav-link"
            href="../../logout.php"
          >
            <i class="fa fa-times-circle"></i>
            Sair
          </a>
        </li>
        &nbsp&nbsp&nbsp
        <li class="nav-item">
         <b><font color="white">Usuário:&nbsp&nbsp<?php echo strtoupper("$userlogado"); ?> </font></b>
        </li>

      </ul>
    </div>
  </nav>

  <!-- Fim do desenho do menu -->
  <!-- Desenho do cadastro -->
  <br>
  <h3>&nbsp&nbsp<?php echo $nome1; ?></h3>
  <br>
  <div align="center">
  <?php
  if ($alert == 1){
    echo $msg;
  }  
  ?>
  </div>
  <input  name="operacao" type="hidden" value='parametros'/>
  <div class="col-md-10">
  <div class="form-row">
  <div class="form-group col-md-4">
  <label for="cli_pad">Cliente padrão mesas</label>
      <select id="cli_pad" class="form-control form-control-sm" name="cli_pad">
       <?php
         echo $htmlselect;
       ?>
      </select>
  </div>
  <div class="form-group col-md-3">
      <label>Taxa de serviço</label>
      <input type="numeric" class="form-control form-control-sm"  id="taxas" value="<?php echo $taxas; ?>" name="taxas" maxlength="5" onkeypress='return SomenteNumero(event)'>
    </div>
</div>
<div class="form-row">
  <div class="form-group col-md-3">
    <label>Controle de atendimento</label>
    <select id="individual" class="form-control form-control-sm" name="individual">
      <option value="t" <?php echo $individual == 't' ? "selected" : ""; ?> >Sim</option>
      <option value="f" <?php echo $individual == 'f' ? "selected" : ""; ?> >Não</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label>Imagem tela principal</label>
    <input id="imagem" type="file" class="form-control form-control-sm" name="imagem" aria-label="Username" aria-describedby="sizing-addon1" maxlength="40">
</div>
</div>
        <button class="btn btn-secondary"  type="submit">Salvar</button>
        <button class="btn btn-secondary" type="reset">Limpar</button>
        <a href="configuracoes.php"><button class="btn btn-secondary" type="button">Voltar</button></a>
     </div>
  </form>
</body>
</html>